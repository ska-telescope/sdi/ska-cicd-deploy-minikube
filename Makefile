# Build a Minikube environment
#
SHELL=/usr/bin/env bash
MINIKUBE_HOME ?= $(HOME)
PROFILE ?= minikube ## The Minikube profile to boot with
PROXY_CONFIG=$(MINIKUBE_HOME)/.minikube/minikube-nginx-haproxy.cfg
HAPROXY_NAME=minikube-nginx-haproxy
OS_NAME := $(shell uname -s | tr A-Z a-z)
OS_ARCH := $(shell uname -p)
OS_BIN := amd64
ifeq ($(OS_NAME),darwin)
ifeq ($(OS_ARCH),arm)
OS_BIN := $(shell uname -m)
endif
endif
# Differentiate how to retrieve the IP address and the hostname
# on macOS or Linux
ifneq ($(OS_NAME),darwin)
IP=$(shell (ip a 2> /dev/null || ifconfig) | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
HOST=$(shell hostname -f)
else
IP=$(shell scutil --nwi | grep 'address' | cut -d':' -f2 | tr -d ' ' | head -n1)
HOST=$(shell hostname -s)
endif
# Kyverno do something different
KYVERNO_ARCH := $(OS_ARCH)
ifeq ($(OS_NAME),darwin)
ifeq ($(OS_ARCH),i386)
KYVERNO_ARCH := $(shell uname -m)
endif
ifeq ($(OS_ARCH),arm)
KYVERNO_ARCH := $(OS_BIN)
endif
endif
EXE_DIR ?= $(shell which minikube 2>/dev/null | sed s=/minikube$$==)## The directory where binaries will be installed
ifeq (,$(EXE_DIR))
  EXE_DIR = /usr/local/bin
endif
BIN_DIR ?= /usr/bin## the directory where the $(DRIVER) is installed eg: docker/podman
CI_JOB_ID ?= local
# cache from https://github.com/rpardini/docker-registry-proxy
# ca cert https://minikube.sigs.k8s.io/docs/handbook/vpn_and_proxy/
CACHE_NAME=registry_cache
REGISTRIES=k8s.gcr.io gcr.io ghcr.io registry.k8s.io quay.io registry.gitlab.com docker.elastic.co artefact.skao.int harbor.skao.int
CACHE_DATA=$(MINIKUBE_HOME)/.minikube/$(CACHE_NAME)
REGISTRY_NAME=registry

# Minkube start parameters
NODES ?= 1## Number of Nodes for Minikube to create - should always be 1
CPUS ?= 4## Number of (v)CPUs to allocate to Minikube
MEM ?= 8192## Amount of RAM to allocate to Minikube (MB)
ADD_ARGS ?= ## Additional ARGS passed to minikube start
HOSTNAME ?= $(HOST)
FQDN ?= $(HOST).local.net
CLUSTER_DOMAIN ?= cluster.local ## The cluster domain
IPADDR ?= $(IP)
TOOLS ?= yes # Install tools such as haproxy and registry
# Something for the future: turns on KubeletConfiguration cpuCFSQuota=false
# --extra-config=kubelet.cpuCFSQuota=false - doesn't work
SUDO_COMMAND = sudo --preserve-env=http_proxy --preserve-env=https_proxy
SUDO_FOR_EXE_DIR = $(shell if [ ! -w $(EXE_DIR) ]; then echo $(SUDO_COMMAND); fi)
MUST_SUDO :=
# need something other than /srv for macOS
ifeq ($(OS_NAME),darwin)
# Differentiate between macOS on arm or intel
ifeq ($(OS_ARCH),arm)
DRIVER ?= docker
else
DRIVER ?= hyperkit
endif
MOUNT_FROM_DIR=$(HOME)
else
DRIVER ?= podman## Virtualisation layer used to host minikube - default podman for Linux, and hyperkit or docker for MacOS
# minikube recommends using the cri-o runtime when using podman, but we ran into
# https://github.com/kubernetes/minikube/issues/12684. That's reported fixed in
# #12705 so we can try again when minikube > 1.24 is released.
RUNTIME ?= docker## How containers are run within minikube - docker, cri-o or containerd
HAPROXY_ENGINE ?= $(RUNTIME)## haproxy container engine for local host deployment of proxy

MOUNT_FROM_DIR=/srv
endif
KIND_DRIVER ?= docker## Kind driver passed to KIND_EXPERIMENTAL_PROVIDER

MOUNT_FROM ?= $(MOUNT_FROM_DIR)## Source directory to mount into Minikube VM(or CnC)

ifeq ($(DRIVER),podman)
MUST_SUDO := $(SUDO_COMMAND)
endif
ADDONS ?= metrics-server ingress
ifeq ($(DRIVER),kvm2)
MINIKUBE_NETWORK ?= host## Minikube network name (Docker Networking)
else
MINIKUBE_NETWORK ?= $(PROFILE)## Minikube network name (Docker Networking)
endif
CILIUM ?= ## add CILIUM=yes to use Cilium as the CNI - 1.16.3
CILIUM_CLUSTER_ID ?= 1## Cilium cluster ID
# CILIUM_VERSION ?= v1.15.6## Cilium CNI version to install https://github.com/cilium/cilium/releases
CILIUM_VERSION ?= v1.16.4## Cilium CNI version to install https://github.com/cilium/cilium/releases

GATEWAY_API_VERSION ?= v1.2.1## https://github.com/kubernetes-sigs/gateway-api/releases
MINIKUBE_VERSION ?= v1.34.0## Minikube tool version to install https://github.com/kubernetes/minikube/releases
KUBERNETES_VERSION ?= v1.31.4## Kubernetes version to bootup https://kubernetes.io/releases/ , https://hub.docker.com/r/kindest/node/tags
KUBERNETES_SERVER_VERSION ?= $(KUBERNETES_VERSION)## Kubernetes server version to be used. Normally a sane default is automatically chosen.
HELM_VERSION ?= v3.16.4## Helm tool version to install https://github.com/helm/helm/releases
HELMFILE_VERSION ?= 0.169.2## Helmfile tool version to install https://github.com/helmfile/helmfile/releases
METALLB_VERSION ?= 0.14.9## MetalLB LoadBalancer version to install https://github.com/metallb/metallb/releases
K9S_VERSION ?= v0.32.7## k9s tool version to install https://github.com/derailed/k9s/releases
YQ_VERSION ?= 4.44.6## yq tool version to install https://github.com/mikefarah/yq/releases
PROXY_VERSION ?= 3.1.0## haproxy version to install for frontend proxy https://hub.docker.com/_/haproxy/tags
MOUNT_TO ?= /srv##Target directory to mount into Minikube VM(or CnC)
FLAGS ?= ## Additional flags to pass to minikube
USE_CACHE ?= ## Deploy a local NGiNX based image cache
CACHE_VERSION ?= 0.6.4## local registry cache based on NGINX https://hub.docker.com/r/rpardini/docker-registry-proxy/tags
KYVERNO_VERSION ?= 1.13.2## Kyverno cli version to install https://github.com/kyverno/kyverno/releases
CILIUM_CLI_VERSION ?= 0.16.22## Cilium cli stable version to install https://raw.githubusercontent.com/cilium/cilium-cli/main/stable.txt
CALICO_VERSION ?= 3.29.1## Calico CNI version https://github.com/projectcalico/calico/releases
TIGERA_VERSION ?= 1.36.3## Calico Tiagera version https://github.com/tigera/operator/releases
KIND_VERSION ?= v0.26.0## Kind tool version to install https://github.com/kubernetes-sigs/kind/releases

UPDATE_LOCAL_DNS ?= true
EXTDNS_NAMESPACE = extdns

K8S_CHARTS ?= ska-tango-base ## Charts to parse for images to preload
K8S_CHART_PARAMS ?= ## Parameters to pass to helm template for image discovery

SKA_TANGO_OPERATOR_NAMESPACE ?= ska-tango-operator
SKA_TANGO_OPERATOR_PING_SERVICE ?= ska-tango-operator-ska-tango-ping
SKA_TANGO_OPERATOR_ENABLED ?= true
SKA_TANGO_OPERATOR_VERSION?=0.10.1 # https://artefact.skao.int/#browse/browse:helm-internal:ska-tango-operator DO NOT USE 10.1.2
VAULT_ENABLED?=false
SKA_TANGO_OPERATOR_PARAMS?= --set ska-tango-ping.apiKeySecret.secretProviderEnabled=$(VAULT_ENABLED)
SKA_TANGO_OPERATOR_CHART ?= skatelescope/ska-tango-operator
SKA_TANGO_PING_CERT_DIR ?= build/ska-tango-ping-certs

ifneq ($(SKA_TANGO_OPERATOR_VERSION),)
	SKA_TANGO_OPERATOR_PARAMS += --version $(SKA_TANGO_OPERATOR_VERSION) \
		--set controllerManager.manager.image.tag=$(SKA_TANGO_OPERATOR_VERSION)
endif

KYVERNO_NAMESPACE ?= kyverno
KYVERNO ?= yes # Install Kyverno

# include core makefile targets for release management
-include .make/base.mk

# include targets for installation, integration and management of Vault
-include scripts/vault.mk

# include targets for using Kind as an alternative to Minikube
-include scripts/kind.mk

# include targets for installation and simple test of Slinky - Slurm Operator
-include scripts/slurm.mk

# include targets for installation and simple test of Flux CD
-include scripts/flux.mk

# include targets for installation and simple test of Kueue
-include scripts/kueue.mk

# include targets for simple test of Cilium cluster mesh
-include scripts/multicluster.mk

# Change the default goal so that we only get the core help
.DEFAULT_GOAL := minikube-localhelp

minikube-localhelp:
	@echo "preparing the help"
	@make help Makefile

# define overides for above variables in here
-include PrivateRules.mak

# cri-o does not currently work with driver=docker
ifeq ($(DRIVER),docker)
RUNTIME := docker
endif

.PHONY: all bats-install bats-reinstall bats-test bats-uninstall clean-dns clean cluster-cache cluster-kubeconfig \
cluster-kyverno cluster-patch-for-metallb cluster-registry cluster-setup-ska-tango-operator cluster-setup \
cluster-ska-tango-operator cluster-storage-classes cluster-test config-extdns extdns-ip extdns-ping \
metallb-config minikube-clean-cache minikube-clean-dns minikube-clean minikube-dig-extdns minikube-extdns \
minikube-haproxy minikube-install-podman minikube-install minikube-ip minikube-load-images minikube-localhelp \
minikube-metallb-config minikube-node-ports minikube-patch-for-cilium minikube-proxy-config minikube-reset-docker-env \
minikube-tools minikube-vars minikube patch-ingress update-dns

minikube-vars: minikube-ip ## list the public vars and Minikube status
	@echo 'Minikube Installed:        $(shell if [ ! -f "$(EXE_DIR)/minikube" ]; then echo "No!"; else echo "Yes!"; fi)'
	@echo 'Helm Installed:            $(shell if [ ! -f "$(EXE_DIR)/helm" ]; then echo "No!"; else echo "Yes!"; fi)'
	@echo "Profile:                   $(PROFILE)"
	@echo "DRIVER:                    $(DRIVER)"
	@echo "RUNTIME:                   $(RUNTIME)"
	@echo "HAPROXY_ENGINE:            $(HAPROXY_ENGINE)"
	@echo "ADD_ARGS:                  $(ADD_ARGS)"
	@echo "ADDONS:                    $(ADDONS)"
	@echo "TOOLS:                     $(TOOLS)"
	@echo "CILIUM:                    $(CILIUM)"
	@echo "CILIUM_VERSION:            $(CILIUM_VERSION)"
	@echo "KYVERNO:                   $(KYVERNO)"
	@echo "CPUS:                      $(CPUS)"
	@echo "MEM:                       $(MEM)"
	@echo "OS_NAME:                   $(OS_NAME)"
	@echo "OS_ARCH:                   $(OS_ARCH)"
	@echo "OS_BIN:                    $(OS_BIN)"
	@echo "EXE_DIR:                   $(EXE_DIR)"
	@echo "SUDO_FOR_EXE_DIR:          $(SUDO_FOR_EXE_DIR)"
	@echo "IPADDR:                    $(IPADDR)"
	@echo "CLUSTER_IP:                $(CLUSTER_IP)"
	@echo "HOSTNAME:                  $(HOSTNAME)"
	@echo "CLUSTER_DOMAIN:            $(CLUSTER_DOMAIN)"
	@echo "FQDN:                      $(FQDN)"
	@echo "MOUNT_FROM:                $(MOUNT_FROM)"
	@echo "MOUNT_TO:                  $(MOUNT_TO)"
	@echo "PROXY_VERSION:             $(PROXY_VERSION)"
	@echo "PROXY_CONFIG:              $(PROXY_CONFIG)"
	@echo "MINIKUBE_VERSION:          $(MINIKUBE_VERSION)"
	@echo "MINIKUBE_NETWORK:          $(MINIKUBE_NETWORK)"
	@echo "KUBERNETES_VERSION:        $(KUBERNETES_VERSION)"
	@echo "KUBERNETES_SERVER_VERSION: $(KUBERNETES_SERVER_VERSION)"
	@echo "HELM_VERSION:              $(HELM_VERSION)"
	@echo "HELMFILE_VERSION:          $(HELMFILE_VERSION)"
	@echo "YQ_VERSION:                $(YQ_VERSION)"
	@echo "KYVERNO_VERSION:           $(KYVERNO_VERSION)"
	@echo "CILIUM_CLI_VERSION:        $(CILIUM_CLI_VERSION)"
	@echo "INGRESS:                   $(INGRESS)"
	@echo "USE_CACHE:                 $(USE_CACHE)"
	@echo "CACHE_DATA:                $(CACHE_DATA)"
	@echo "KIND_VERSION:              $(KIND_VERSION)"
	@echo "KIND_DRIVER:               $(KIND_DRIVER)"
	@echo "Minikube status:"
	@$(EXE_DIR)/minikube --profile $(PROFILE) status 2>/dev/null | grep -v ' fix ' || true
ifeq ($(strip $(CILIUM)),yes)
	@echo "Cilium status:"
	@cilium status
endif

# inject the CLUSTER_IP and INGRESS
minikube-ip:
# check for minikube running in WSL2
# linux/OSX
ifeq ($(origin WSLENV),undefined)
	$(eval CLUSTER_IP := $(shell $(EXE_DIR)/minikube --profile $(PROFILE) ip 2>/dev/null | grep -v ' fix '))
else
# WSL2
# minikube creates its own docker bridge network 'minikube', but
# minikube ip returns 127.0.0.1 .This might be a bug in the WSL2 minikube
# implementation. Manually retrieve the external docker network ip.
# Note this call will fail until make minikube has been called
	$(eval CLUSTER_IP := $(shell unset DOCKER_TLS_VERIFY DOCKER_HOST DOCKER_CERT_PATH MINIKUBE_ACTIVE_DOCKERD; \
					$(MUST_SUDO) $(BIN_DIR)/$(DRIVER) inspect -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' minikube 2>/dev/null))
endif
# The ingress controller addon is automatically assigned the 'minikube ip' on port 80
	$(eval INGRESS := "http://$(CLUSTER_IP)")

minikube-node-ports: ## list potential node ports from the running Minikube cluster
	@NODE_PORTS="$$(kubectl --context $(PROFILE) get svc -A  | grep NodePort | awk '{print $$6}' | sed 's/,/ /g' | sed 's/\/TCP//g' | paste -sd " " -)" && echo "NODE_PORTS=$${NODE_PORTS}"

minikube-proxy-config:
	@echo "$${HAPROXY_CONFIG}" | envsubst; \
	NODE_PORTS="$$(kubectl --context $(PROFILE) get svc -A | grep NodePort | awk '{print $$6}' | sed 's/,/ /g' | sed 's/\/TCP//g' | paste -sd " " -)" && \
	if [ -n "$${NODE_PORTS}" ]; then \
	for i in $${NODE_PORTS}; do \
		export PORT=$$(echo "$$i" | sed 's/.*://'); echo "Adding proxy for NodePort $${PORT}"; echo "$${ADD_HAPROXY_CONFIG}" | sed "s/XPORTX/$${PORT}/g" >> $(PROXY_CONFIG) ; \
	done; \
	fi

# if you have used "minikube docker-env" to set docker env vars to peek inside minikube,
# we need to undefine them for this Makefile to work.
# Note that their values will be retained for the user (outside the Makefile).
minikube-reset-docker-env:
	$(eval undefine DOCKER_TLS_VERIFY)
	$(eval undefine DOCKER_HOST)
	$(eval undefine DOCKER_CERT_PATH)
	$(eval undefine MINIKUBE_ACTIVE_DOCKERD)

# must checkout .make submodule
ifeq (,$(wildcard .make/help.mk))
$(shell git submodule update --init)
endif

# define custom targets
-include PrivateTargets.mak

all: cluster-setup minikube-install ## 🔥 do setup and install

minikube-clean: minikube-reset-docker-env ## 💀 Delete Minikube cluster etc. - **caution** there is no going back!
	@make minikube-clean-dns PROFILE=$(PROFILE)
	@echo "Cleaning up with DRIVER: $(DRIVER)"
ifneq ($(OS_NAME),darwin)
	@$(MUST_SUDO) $(BIN_DIR)/docker rm -f $(CACHE_NAME) 1>/dev/null 2>&1 || true
	@$(MUST_SUDO) $(BIN_DIR)/podman rm -f $(CACHE_NAME) 1>/dev/null 2>&1 || true
	@$(MUST_SUDO) $(BIN_DIR)/docker rm -f $(REGISTRY_NAME) 1>/dev/null 2>&1 || true
	@$(MUST_SUDO) $(BIN_DIR)/podman rm -f $(REGISTRY_NAME) 1>/dev/null 2>&1 || true
	@$(MUST_SUDO) $(BIN_DIR)/docker rm -f $(HAPROXY_NAME) 1>/dev/null 2>&1 || true
	@$(MUST_SUDO) $(BIN_DIR)/podman rm -f $(HAPROXY_NAME) 1>/dev/null 2>&1 || true
ifeq ($(DRIVER),kvm2)
	@$(MUST_SUDO) $(BIN_DIR)/docker rm -f $(HAPROXY_NAME) 1>/dev/null 2>&1 || true
	@$(MUST_SUDO) $(BIN_DIR)/docker rm -f $(REGISTRY_NAME) 2>/dev/null || true
else
	@$(MUST_SUDO) $(BIN_DIR)/$(DRIVER) rm -f $(HAPROXY_NAME) 1>/dev/null 2>&1 || true
	@$(MUST_SUDO) $(BIN_DIR)/$(DRIVER) rm -f $(REGISTRY_NAME) 2>/dev/null || true
	@$(MUST_SUDO) $(BIN_DIR)/podman rm -f $(HAPROXY_NAME) 1>/dev/null 2>&1 || true
	@$(MUST_SUDO) $(BIN_DIR)/podman rm -f $(REGISTRY_NAME) 2>/dev/null || true
endif
endif
	@$(EXE_DIR)/minikube --profile $(PROFILE) stop || true
	@$(EXE_DIR)/minikube --profile $(PROFILE) delete || true
	@rm -rf $(MINIKUBE_HOME)/.minikube/files/etc/ssl/certs/proxy.crt

clean: minikube-clean ## alias for minikube-clean

minikube-clean-cache: ## 💀 clean the cache - WARNING you must make minikube-clean Minikube first!!!!
	@$(MUST_SUDO) $(BIN_DIR)/$(DRIVER) rm -f $(CACHE_NAME) 1>/dev/null 2>&1 || true
	@sudo rm -rf $(CACHE_DATA) || true

minikube-install-podman: ## ✨ Install podman tools, if this Makefile supports your OS.
ifeq ($(OS_NAME),darwin)
	brew install podman
	podman machine init --cpus 2 --memory 2048 --disk-size 20
	podman machine start
	podman system connection default podman-machine-default-root
	podman info
else ifeq ($(shell if [ "$(OS_NAME)" = "linux" ] && which apt &> /dev/null; then echo ok; fi),ok)
	$(SUDO_COMMAND) apt update -y
	$(SUDO_COMMAND) apt install -y podman buildah skopeo
else
	echo "Unsupported OS"
	exit 1
endif

cluster-setup: ## ✨ install command line tools for Minikube, kubectl, helm, k9s and yq dependencies
	$(eval RIGHT_MINIKUBE := $(shell $(EXE_DIR)/minikube version --short 2>&1 | grep $(MINIKUBE_VERSION)))
	@if [ ! -f "$(EXE_DIR)/minikube" ] || [ -z "$(RIGHT_MINIKUBE)" ]; then \
		echo "Installing minikube version $(MINIKUBE_VERSION)"; \
		$(SUDO_FOR_EXE_DIR) curl -Lo $(EXE_DIR)/minikube https://github.com/kubernetes/minikube/releases/download/$(MINIKUBE_VERSION)/minikube-$(OS_NAME)-$(OS_BIN); \
		$(SUDO_FOR_EXE_DIR) chmod +x $(EXE_DIR)/minikube; \
	else \
		echo "Minikube $(MINIKUBE_VERSION) already installed"; \
	fi

	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/kubectl)
	$(eval RIGHT_KUBECTL := $(shell $(EXE_DIR)/kubectl version --client -o yaml | grep gitVersion | awk '{print $2}' | grep $(KUBERNETES_VERSION)))
	@if [ ! -f "$(EXE_DIR)/kubectl" ] || [ -z "$(RIGHT_KUBECTL)" ]; then \
		echo "Installing kubectl version $(KUBERNETES_VERSION)"; \
		curl -Lo $(TMP_FILE) https://dl.k8s.io/release/$(KUBERNETES_VERSION)/bin/$(OS_NAME)/$(OS_BIN)/kubectl \
		&& $(SUDO_FOR_EXE_DIR) mv -f $(TMP_FILE) $(EXE_DIR)/kubectl  \
		&& $(SUDO_FOR_EXE_DIR) chmod +x $(EXE_DIR)/kubectl; \
	else \
		echo "kubectl $(KUBERNETES_VERSION) already installed"; \
	fi
	@rm -rf $(TEMPDIR)

	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/helm.tar.gz)
	$(eval RIGHT_HELM := $(shell $(EXE_DIR)/helm version --short 2>&1 | grep $(HELM_VERSION)))
	@if [ ! -f "$(EXE_DIR)/helm" ] || [ -z "$(RIGHT_HELM)" ]; then \
		echo "Installing helm version $(HELM_VERSION)"; \
		curl -Lo $(TMP_FILE) https://get.helm.sh/helm-$(HELM_VERSION)-$(OS_NAME)-$(OS_BIN).tar.gz \
		&& tar -xzOf $(TMP_FILE) $(OS_NAME)-$(OS_BIN)/helm > /tmp/helm && $(SUDO_FOR_EXE_DIR) mv -f /tmp/helm $(EXE_DIR)/helm  \
		&& $(SUDO_FOR_EXE_DIR) chmod +x $(EXE_DIR)/helm; \
	else \
		echo "helm $(HELM_VERSION) already installed"; \
	fi
	@rm -rf $(TEMPDIR)

	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/helmfile.tar.gz)
	$(eval RIGHT_HELMFILE := $(shell $(EXE_DIR)/helmfile version -o=yaml 2>&1 | grep "version: $(HELMFILE_VERSION)"))
	@if [ ! -f "$(EXE_DIR)/helmfile" ] || [ -z "$(RIGHT_HELMFILE)" ]; then \
		echo "Installing helmfile version $(HELMFILE_VERSION)"; \
		curl -Lo $(TMP_FILE) https://github.com/helmfile/helmfile/releases/download/v$(HELMFILE_VERSION)/helmfile_$(HELMFILE_VERSION)_$(OS_NAME)_$(OS_BIN).tar.gz \
		&& tar -xzOf $(TMP_FILE) helmfile > /tmp/helmfile && $(SUDO_FOR_EXE_DIR) mv -f /tmp/helmfile $(EXE_DIR)/helmfile  \
		&& $(SUDO_FOR_EXE_DIR) chmod +x $(EXE_DIR)/helmfile; \
	else \
		echo "helmfile $(HELMFILE_VERSION) already installed"; \
	fi
	@rm -rf $(TEMPDIR)

	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/k9s.tar.gz)
	$(eval RIGHT_K9S := $(shell $(EXE_DIR)/k9s version --short 2>&1 | grep $(K9S_VERSION)))
	@UPPER_OS_NAME="$$(echo "$(OS_NAME)" | sed 's/.*/\u&/')" && \
	if [ ! -f "$(EXE_DIR)/k9s" ] || [ -z "$(RIGHT_K9S)" ]; then \
		echo "Installing k9s version $(K9S_VERSION) (https://github.com/derailed/k9s/releases/download/$(K9S_VERSION)/k9s_$${UPPER_OS_NAME}_$(OS_BIN).tar.gz)"; \
		curl -Lo $(TMP_FILE) https://github.com/derailed/k9s/releases/download/$(K9S_VERSION)/k9s_$${UPPER_OS_NAME}_$(OS_BIN).tar.gz \
			&& tar -xzOf $(TMP_FILE) k9s > /tmp/k9s && $(SUDO_FOR_EXE_DIR) mv -f /tmp/k9s $(EXE_DIR)/k9s  \
			&& $(SUDO_FOR_EXE_DIR) chmod +x $(EXE_DIR)/k9s; \
	else \
		echo "K9s $(K9S_VERSION) already installed"; \
	fi
	@rm -rf $(TEMPDIR)

	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/yq)
	$(eval RIGHT_YQ := $(shell $(EXE_DIR)/yq --version 2>&1 | grep $(YQ_VERSION)))
	@if [ ! -f "$(EXE_DIR)/yq" ] || [ -z "$(RIGHT_YQ)" ]; then \
		echo "Installing yq version $(YQ_VERSION) from https://github.com/mikefarah/yq/"; \
		curl -Lo $(TMP_FILE) https://github.com/mikefarah/yq/releases/download/v$(YQ_VERSION)/yq_$(OS_NAME)_$(OS_BIN) && \
		$(SUDO_FOR_EXE_DIR) mv $(TMP_FILE) "$(EXE_DIR)/yq" && \
		$(SUDO_FOR_EXE_DIR) chmod +x "$(EXE_DIR)/yq"; \
	else \
		echo "yq $(YQ_VERSION) already installed"; \
	fi
	@rm -rf $(TEMPDIR)

ifeq ($(strip $(KYVERNO)),yes)
	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/kyverno-cli.tar.gz)
	$(eval RIGHT_KYVERNO := $(shell $(EXE_DIR)/kyverno version 2>&1 | grep "Version: $(KYVERNO_VERSION)"))
	@if [ ! -f "$(EXE_DIR)/kyverno" ] || [ -z "$(RIGHT_KYVERNO)" ]; then \
		echo "Installing kyverno version $(KYVERNO_VERSION)"; \
		curl -Lo $(TMP_FILE) https://github.com/kyverno/kyverno/releases/download/v$(KYVERNO_VERSION)/kyverno-cli_v$(KYVERNO_VERSION)_$(OS_NAME)_$(KYVERNO_ARCH).tar.gz \
		&& tar -xzOf $(TMP_FILE) kyverno > /tmp/kyverno && $(SUDO_FOR_EXE_DIR) mv -f /tmp/kyverno $(EXE_DIR)/kyverno  \
		&& $(SUDO_FOR_EXE_DIR) chmod +x $(EXE_DIR)/kyverno; \
	else \
		echo "kyverno $(KYVERNO_VERSION) already installed"; \
	fi
	@rm -rf $(TEMPDIR)
endif

ifeq ($(strip $(CILIUM)),yes)
	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/cilium-cli.tar.gz)
	$(eval RIGHT_CILIUM := $(shell $(EXE_DIR)/cilium version --client 2>&1 | grep "v$(CILIUM_CLI_VERSION)"))
	@if [ ! -f "$(EXE_DIR)/cilium" ] || [ -z "$(RIGHT_CILIUM)" ]; then \
		echo "Installing cilium version $(CILIUM_CLI_VERSION)"; \
		curl -Lo $(TMP_FILE) https://github.com/cilium/cilium-cli/releases/download/v$(CILIUM_CLI_VERSION)/cilium-linux-$(OS_BIN).tar.gz \
		&& tar -xzOf $(TMP_FILE) cilium > /tmp/cilium && $(SUDO_FOR_EXE_DIR) mv -f /tmp/cilium $(EXE_DIR)/cilium  \
		&& $(SUDO_FOR_EXE_DIR) chmod +x $(EXE_DIR)/cilium; \
	else \
		echo "cilium $(CILIUM_CLI_VERSION) already installed"; \
	fi
	@rm -rf $(TEMPDIR)
endif

	$(eval RIGHT_KIND := $(shell $(EXE_DIR)/kind version 2>&1 | grep $(KIND_VERSION)))
	@if [ ! -f "$(EXE_DIR)/kind" ] || [ -z "$(RIGHT_KIND)" ]; then \
		echo "Installing kind version $(KIND_VERSION)"; \
		$(SUDO_FOR_EXE_DIR) curl -Lo $(EXE_DIR)/kind https://github.com/kubernetes-sigs/kind/releases/download/$(KIND_VERSION)/kind-$(OS_NAME)-$(OS_BIN); \
		$(SUDO_FOR_EXE_DIR) chmod +x $(EXE_DIR)/kind; \
	else \
		echo "Kind $(KIND_VERSION) already installed"; \
	fi

setup: cluster-setup

minikube-install: ## ✨ Install Minikube, Ingress Controller, Metal LB, and HAProxy (on Linux)
	@make cluster-cache PROFILE=$(PROFILE)
	@make minikube PROFILE=$(PROFILE)
ifeq ($(strip $(CILIUM)),yes)
	@make minikube-patch-for-cilium PROFILE=$(PROFILE)
endif

ifneq ($(strip $(ADDONS)),)
	@make minikube-addons PROFILE=$(PROFILE)
endif
	@make cluster-storage-classes PROFILE=$(PROFILE)
	@make cluster-patch-for-metallb PROFILE=$(PROFILE)
	@make minikube-metallb-config PROFILE=$(PROFILE)
ifeq ($(strip $(KYVERNO)),yes)
	@make cluster-kyverno PROFILE=$(PROFILE)
endif
ifeq ($(DRIVER),podman)
	@make patch-ingress PROFILE=$(PROFILE) # patch ingress-controller for podman
endif
	@make minikube-extdns PROFILE=$(PROFILE) # deploy the external DNS responder paired with metallb
	@make update-dns PROFILE=$(PROFILE) # update systemd-resolved config for DNS
ifeq ($(strip $(SKA_TANGO_OPERATOR_ENABLED)),true)
	@make cluster-ska-tango-operator PROFILE=$(PROFILE)
endif
ifeq ($(strip $(TOOLS)),yes)
	@make minikube-tools PROFILE=$(PROFILE) # must run make again to set the variables
endif
	@make minikube-vars PROFILE=$(PROFILE)


minikube-addons: ## install minikube addons
	@for addon in $(ADDONS); do \
		minikube --profile $(PROFILE) addons enable	$${addon}; \
	done

minikube-tools: ## install HAProxy
ifeq ($(OS_NAME),linux)
	@make minikube-haproxy PROFILE=$(PROFILE) # must run make again to set the variables
	@make cluster-registry PROFILE=$(PROFILE)
else
	@echo "Your OS is not supported for haproxy (must be Linux): $(OS_NAME)"
endif

minikube:
ifeq ($(strip $(CILIUM)),yes)
	$(eval CILIUM_ARGS := --network-plugin=cni --cni=false)
endif
	@if [ ! -f "$(EXE_DIR)/minikube" ]; then echo "Minikube not Installed - aborted!"; exit 1; fi
	@echo "Minikube status:"
	@$(EXE_DIR)/minikube --profile $(PROFILE) status; rc=$$?; \
	if  [ $$rc != 0 ]; then \
	echo "Minikube not running, continuing..."; \
	echo "Using driver: $(DRIVER) with runtime: $(RUNTIME)"; \
	mkdir -p $(MOUNT_FROM); \
	if [ -n "$(strip $(USE_CACHE))" ]; then \
		export ENV_VARS=" --docker-env http_proxy=http://$(IPADDR):3128 --docker-env https_proxy=http://$(IPADDR):3128 --docker-env no_proxy=$(IPADDR),localhost,127.0.0.1,10.96.0.0/12,192.168.59.0/24,192.168.39.0/24,172.17.0.1/16 "; \
		mkdir -p $(MINIKUBE_HOME)/.minikube/files/etc/ssl/certs; \
		cp $(CACHE_DATA)/docker_mirror_certs/ca.crt $(MINIKUBE_HOME)/.minikube/files/etc/ssl/certs/proxy.crt; \
	fi; \
	if [ -n "$${http_proxy}$${https_proxy}" ]; then \
		export ENV_VARS=" --docker-env http_proxy=$${http_proxy} --docker-env https_proxy=$${http_proxy} --docker-env no_proxy=$${no_proxy} "; \
	fi; \
	echo "Extra ARGS set: $${ENV_VARS} $(ADD_ARGS)"; \
	echo "Local mount: $(MOUNT_FROM):$(MOUNT_TO)"; \
	$(EXE_DIR)/minikube --profile $(PROFILE) start \
		--driver=$(DRIVER) \
		--container-runtime=$(RUNTIME) \
		--kubernetes-version=$(KUBERNETES_SERVER_VERSION) \
		--cpus=$(CPUS) --memory=$(MEM) $${ENV_VARS} \
		--apiserver-port=6443 \
		--apiserver-names=$(HOSTNAME) --apiserver-names=$(FQDN) \
		--dns-domain=$(CLUSTER_DOMAIN) \
		--apiserver-ips=$(IPADDR) \
		--nodes=$(NODES) \
		$(CILIUM_ARGS) \
		$(ADD_ARGS) \
		--insecure-registry="$(IP):5000" \
		--mount --mount-string="$(MOUNT_FROM):$(MOUNT_TO)" $(FLAGS); \
	else \
		echo -e "\033[1;33mMinikube already running. If this is unexpected try running: make 'minikube-clean && make all'\033[0m"; \
	fi;

minikube-patch-for-cilium:
	@kubectl --context $(PROFILE) apply -f https://raw.githubusercontent.com/kubernetes-sigs/gateway-api/$(GATEWAY_API_VERSION)/config/crd/standard/gateway.networking.k8s.io_gatewayclasses.yaml
	@kubectl --context $(PROFILE) apply -f https://raw.githubusercontent.com/kubernetes-sigs/gateway-api/$(GATEWAY_API_VERSION)/config/crd/standard/gateway.networking.k8s.io_gateways.yaml
	@kubectl --context $(PROFILE) apply -f https://raw.githubusercontent.com/kubernetes-sigs/gateway-api/$(GATEWAY_API_VERSION)/config/crd/standard/gateway.networking.k8s.io_httproutes.yaml
	@kubectl --context $(PROFILE) apply -f https://raw.githubusercontent.com/kubernetes-sigs/gateway-api/$(GATEWAY_API_VERSION)/config/crd/experimental/gateway.networking.k8s.io_referencegrants.yaml
	@sleep 3
	@cilium install \
		--context $(PROFILE) \
		--namespace kube-system \
		--set cluster.name=$(PROFILE) \
		--set cluster.id=$(CILIUM_CLUSTER_ID) \
		--set gatewayAPI.enabled=true \
		--set cni.exclusive=false \
		--set clustermesh.enableEndpointSliceSynchronization=true
	@sleep 3
	@kubectl --context $(PROFILE) patch clusterrole cilium-operator \
	--type='json' \
	-p='[{"op": "add", "path": "/rules/0", "value":{ "apiGroups": [""], "resources": ["pods"], "verbs": ["list","watch"]}}]'

# See: https://metallb.universe.tf/installation/
cluster-patch-for-metallb:
	@kubectl --context $(PROFILE) get configmap kube-proxy -n kube-system -o yaml | \
		sed -e "s/strictARP: false/strictARP: true/" | \
		kubectl --context $(PROFILE) apply -f - -n kube-system
	@for i in `kubectl --context $(PROFILE) get nodes -o jsonpath='{.items[*].metadata.name}'`; do \
	  kubectl --context $(PROFILE) label node $$i node.kubernetes.io/exclude-from-external-load-balancers- ; \
	done

cluster-storage-classes:
	@echo "Apply the standard storage classes"
	kubectl --context $(PROFILE) apply -f ./resources/sc.yaml


minikube-metallb-config: minikube-ip metallb-config

metallb-config:
	@echo "Install metallb"
	kubectl --context $(PROFILE) apply -f https://raw.githubusercontent.com/metallb/metallb/v$(METALLB_VERSION)/config/manifests/metallb-native.yaml
	@echo "Wait for metallb to be ready"
	@kubectl --context $(PROFILE) wait --for=condition=ready pod --all --timeout=180s -n metallb-system
	$(eval IP_PREFIX := $(shell echo "$(CLUSTER_IP)" | sed 's/.[0-9]*$$//'))
	@echo "Apply the metallb address pool and L2 advertisement - prefix: $(IP_PREFIX)"
	@kubectl --context $(PROFILE) create ns metallb-system --dry-run=client -o yaml | kubectl --context $(PROFILE) apply -f -
	@echo "$${ADD_METALLB_CONFIG}" | envsubst | kubectl --context $(PROFILE) apply -f -

minikube-extdns: minikube-ip config-extdns ## Deploy CoreDNS k8s_external responder

config-extdns: ## Deploy CoreDNS k8s_external responder
	@echo "Add the coredns chart"
	@helm repo add coredns https://coredns.github.io/helm
	@helm repo update coredns
	@echo "Setup coredns k8s_external responder"
	$(eval IP_PREFIX := $(shell echo "$(CLUSTER_IP)" | sed 's/.[0-9]*$$//'))
	@echo "Apply the metallb config map - prefix: $(IP_PREFIX)"
	@echo "$${COREDNS_EXTERNAL_CONFIG}" | envsubst > coredns-values.yaml
	@helm --kube-context $(PROFILE) --namespace=$(EXTDNS_NAMESPACE) upgrade --install --wait --create-namespace extdns coredns/coredns -f coredns-values.yaml
	@rm -f coredns-values.yaml
	@echo "Waiting for external DNS responder to start"
	@kubectl --context $(PROFILE) --namespace=$(EXTDNS_NAMESPACE) wait --for=condition=ready --timeout=360s $$(kubectl --context $(PROFILE) --namespace=$(EXTDNS_NAMESPACE) get pod -l app.kubernetes.io/instance=extdns -o name)
ifneq ($(OS_NAME),darwin)
	@export SERVICE_IP=$$(kubectl --context $(PROFILE) get svc --namespace $(EXTDNS_NAMESPACE) extdns-coredns -o jsonpath='{.status.loadBalancer.ingress[0].ip}'); \
	echo "CoreDNS with k8s_external plugin is listening on: $$SERVICE_IP:53"; \
	dig @$$SERVICE_IP -p 53 extdns-coredns.$(EXTDNS_NAMESPACE).svc.$(CLUSTER_DOMAIN)
endif

extdns-ping: ## Deploy CoreDNS k8s_external responder
	export SERVICE_IP=$$(kubectl --context $(PROFILE) get svc --namespace $(EXTDNS_NAMESPACE) extdns-coredns -o jsonpath='{.status.loadBalancer.ingress[0].ip}'); \
	echo "CoreDNS with k8s_external plugin is listening on: $$SERVICE_IP:53"; \
	dig @$$SERVICE_IP -p 53 extdns-coredns.$(EXTDNS_NAMESPACE).svc.$(CLUSTER_DOMAIN)


extdns-ip:
	$(eval EXTDNS_IP := $(shell kubectl --context $(PROFILE) get svc --namespace $(EXTDNS_NAMESPACE) extdns-coredns -o jsonpath='{.status.loadBalancer.ingress[0].ip}'))
	@echo "CoreDNS with k8s_external plugin is listening on: $(EXTDNS_IP):53"

minikube-dig-extdns: extdns-ip ## Dig CoreDNS k8s_external responder
	@export SERVICE_IP=$$(kubectl --context $(PROFILE) get svc --namespace $(EXTDNS_NAMESPACE) extdns-coredns -o jsonpath='{.status.loadBalancer.ingress[0].ip}'); \
	echo "CoreDNS with k8s_external plugin is listening on: $$SERVICE_IP:53"; \
	dig @$$SERVICE_IP -p 53 extdns-coredns.$(EXTDNS_NAMESPACE).svc.$(CLUSTER_DOMAIN)

update-dns: extdns-ip
ifeq ($(UPDATE_LOCAL_DNS),true)
ifeq ($(OS_NAME),linux)
	@$(SUDO_COMMAND) sed -i.x -e '/svc.$(CLUSTER_DOMAIN)/d' /etc/systemd/resolved.conf
	@$(SUDO_COMMAND) rm -f /etc/systemd/resolved.conf.x
	$(eval IP_PREFIX := $(shell echo "$(CLUSTER_IP)" | sed 's/.[0-9]*$$//'))
	@$(SUDO_COMMAND) sed -i.x -e '/DNS=$(IP_PREFIX)/d' /etc/systemd/resolved.conf
	@$(SUDO_COMMAND) rm -f /etc/systemd/resolved.conf.x
	@$(SUDO_COMMAND) sed -i.x -e 's/^DNS=/#DNS=/' /etc/systemd/resolved.conf
	@$(SUDO_COMMAND) rm -f /etc/systemd/resolved.conf.x
	@echo -e "DNS=$(EXTDNS_IP)\nDomains=~svc.$(CLUSTER_DOMAIN)\n" | $(SUDO_COMMAND) tee -a /etc/systemd/resolved.conf > /dev/null
	@$(SUDO_COMMAND) systemctl restart systemd-resolved
	@if [ -x "$$(command -v resolvconf)" ]; then \
		$(SUDO_COMMAND) resolvconf -u 2>/dev/null || true; \
		dig extdns-coredns.extdns.svc.$(CLUSTER_DOMAIN) ; \
	else \
		echo "❗ WARNING: resolvconf not install, so DNS configuration may not be updated correctly"; \
		echo "👉 INFO: You can check DNS with dig extdns-coredns.extdns.svc.$(CLUSTER_DOMAIN) "; \
	fi
else
	@echo "Your OS is not supported for systemd-resolved configuration of DNS (must be Linux): $(OS_NAME)"
	@echo "Update your DNS to include nameserver: $(EXTDNS_IP) and search domain: svc.$(CLUSTER_DOMAIN)"
endif
	@echo "You have disabled automatic update of the local DNS server."
	@echo "Update your DNS to include nameserver: $(EXTDNS_IP) and search domain: svc.$(CLUSTER_DOMAIN)"
endif

minikube-clean-dns: minikube-ip clean-dns

clean-dns: extdns-ip
ifeq ($(OS_NAME),linux)
	@$(SUDO_COMMAND) sed -i.x -e '/svc.$(CLUSTER_DOMAIN)/d' /etc/systemd/resolved.conf
	$(eval IP_PREFIX := $(shell echo "$(CLUSTER_IP)" | sed 's/.[0-9]*$$//'))
	@$(SUDO_COMMAND) sed -i.x -e '/^DNS=$(IP_PREFIX)/d' /etc/systemd/resolved.conf
	@$(SUDO_COMMAND) rm -f /etc/systemd/resolved.conf.x
	@$(SUDO_COMMAND) sed -i.x -e 's/^#DNS=/DNS=/' /etc/systemd/resolved.conf
	@$(SUDO_COMMAND) rm -f /etc/systemd/resolved.conf.x
	@grep 'DNS='  /etc/systemd/resolved.conf >/dev/null || \
	echo -e "#DNS=\n#Domains=\n" | $(SUDO_COMMAND) tee -a /etc/systemd/resolved.conf > /dev/null
	@$(SUDO_COMMAND) systemctl restart systemd-resolved
	@if [ -x "$$(command -v resolvconf)" ]; then \
		$(SUDO_COMMAND) resolvconf -u 2>/dev/null || true; \
	else \
		echo "❗ WARNING: resolvconf not install, so DNS configuration may not be updated correctly"; \
	fi

else
	@echo "Your OS is not supported for systemd-resolved configuration of DNS (must be Linux): $(OS_NAME)"
	@echo "Update your DNS to REMOVE nameserver: $(EXTDNS_IP) and search domain: svc.$(CLUSTER_DOMAIN)"
endif

# patch ingress controller for podman
patch-ingress:
	@echo "Patch Ingress Controller for permissions issues:"
	@kubectl --context $(PROFILE) -n ingress-nginx scale deployment/ingress-nginx-controller --replicas=0
	@kubectl --context $(PROFILE)  -n ingress-nginx delete $(kubectl --context $(PROFILE) -n ingress-nginx get pods -l app.kubernetes.io/component=controller -o NAME)  &>/dev/null || true
	@kubectl --context $(PROFILE) -n ingress-nginx patch deployment/ingress-nginx-controller \
	--type json   -p='[{"op": "replace", "path": "/spec/template/spec/containers/0/securityContext", "value": {"runAsUser": 0, "allowPrivilegeEscalation": true, "capabilities": {"add": ["NET_BIND_SERVICE"]}}}]'
	@echo "Patched NET_BIND_SERVICE"
	@kubectl --context $(PROFILE)  -n ingress-nginx patch configmap/ingress-nginx-controller --type merge \
		-p '{"data":{"worker-processes":"2"}}'
	@echo "Patched worker-processes"
	@sleep 3
	@kubectl --context $(PROFILE) -n ingress-nginx scale deployment/ingress-nginx-controller --replicas=1
	@sleep 10
	@echo "Check that the Ingress Controller is properly started:"
	@kubectl --context $(PROFILE) -n ingress-nginx get pods -l app.kubernetes.io/component=controller
	@kubectl --context $(PROFILE) -n ingress-nginx wait --for=condition=ready $$(kubectl --context $(PROFILE) -n ingress-nginx get pods -l app.kubernetes.io/component=controller -o name)

minikube-haproxy: minikube-reset-docker-env minikube-ip ## Setup haproxy to present the Ingress controller to the outside world
	# Now setup the Proxy to the NGINX Ingress and APIServer, and any NodePort services
	# need to know the device and IP as this must go in the proxy config
	@echo "Installing HAProxy frontend to make Minikube externally addressable"
	@$(MUST_SUDO) $(BIN_DIR)/$(HAPROXY_ENGINE) rm -f $(HAPROXY_NAME) 2>/dev/null || true
	@echo "CLUSTER_IP: $(CLUSTER_IP)" && \
	echo "$${HAPROXY_CONFIG}" | envsubst > $(PROXY_CONFIG); \
	export NODE_PORTS="80:80 443:443 ${NODE_PORTS} "; \
	for i in $${NODE_PORTS}; do \
		export PORT=$$(echo "$$i" | sed 's/.*://'); echo "Adding proxy for NodePort $${PORT}"; echo "$${ADD_HAPROXY_CONFIG}" | sed "s/XPORTX/$${PORT}/g" >> $(PROXY_CONFIG) ; \
	    export PORTS="$${PORTS} -p $${PORT}:$${PORT} "; \
	done; \
	if [[ "$(DRIVER)" == "docker" ]] || [[ "$(DRIVER)" == "kvm2" ]]; then \
	$(MUST_SUDO) $(BIN_DIR)/$(HAPROXY_ENGINE) run --name $(HAPROXY_NAME) --net=$(MINIKUBE_NETWORK) \
		-p 6443:6443 $${PORTS} \
		-v $(PROXY_CONFIG):/usr/local/etc/haproxy/haproxy.cfg \
		-d haproxy:$(PROXY_VERSION) -f /usr/local/etc/haproxy/haproxy.cfg; \
	else \
	$(MUST_SUDO) $(BIN_DIR)/$(HAPROXY_ENGINE) run --name $(HAPROXY_NAME) --sysctl net.ipv4.ip_unprivileged_port_start=0  \
		-p 6443:6443 $${PORTS} \
		-v $(PROXY_CONFIG):/usr/local/etc/haproxy/haproxy.cfg \
		-d docker.io/haproxy:$(PROXY_VERSION) -f /usr/local/etc/haproxy/haproxy.cfg; \
	fi

cluster-setup-ska-tango-operator: ## setup pki for ska-tango-operator's dependencies https://kubernetes.github.io/ingress-nginx/deploy/validating-webhook/
	@kubectl --context $(PROFILE) create ns $(SKA_TANGO_OPERATOR_NAMESPACE) || true
	@rm -rf $(SKA_TANGO_PING_CERT_DIR) && mkdir -p $(SKA_TANGO_PING_CERT_DIR)
	@echo "$${SKA_TANGO_OPERATOR_CSR_CONF}" | envsubst > $(SKA_TANGO_PING_CERT_DIR)/csr.conf;
	@openssl genrsa -out $(SKA_TANGO_PING_CERT_DIR)/tls.key 2048
	@openssl req -new -key $(SKA_TANGO_PING_CERT_DIR)/tls.key \
	-subj "/CN=$(SKA_TANGO_OPERATOR_PING_SERVICE).$(SKA_TANGO_OPERATOR_NAMESPACE).svc" \
	-out $(SKA_TANGO_PING_CERT_DIR)/server.csr \
	-config $(SKA_TANGO_PING_CERT_DIR)/csr.conf
	@export SKA_TANGO_OPERATOR_CSR_B64=$$(cat $(SKA_TANGO_PING_CERT_DIR)/server.csr | base64 | tr -d '\n'); \
	echo "$${SKA_TANGO_OPERATOR_SIGN_REQUEST}" | envsubst > $(SKA_TANGO_PING_CERT_DIR)/cert-approve.yaml;
	@kubectl --context $(PROFILE) delete -f $(SKA_TANGO_PING_CERT_DIR)/cert-approve.yaml || true
	@kubectl --context $(PROFILE) apply -f $(SKA_TANGO_PING_CERT_DIR)/cert-approve.yaml
	@sleep 3
	@kubectl --context $(PROFILE) certificate approve $(SKA_TANGO_OPERATOR_PING_SERVICE)-csr
	@openssl req -x509 -newkey rsa:2048 -keyout $(SKA_TANGO_PING_CERT_DIR)/tls.key -out $(SKA_TANGO_PING_CERT_DIR)/tls.crt -days 3650 \
	-nodes -subj "/CN=$(SKA_TANGO_OPERATOR_PING_SERVICE).$(SKA_TANGO_OPERATOR_NAMESPACE).svc"
	@kubectl --context $(PROFILE) create secret generic server-cert \
	--save-config \
	--dry-run=client \
	--from-file=tls.key=$(SKA_TANGO_PING_CERT_DIR)/tls.key \
	--from-file=tls.crt=$(SKA_TANGO_PING_CERT_DIR)/tls.crt \
	-n $(SKA_TANGO_OPERATOR_NAMESPACE) \
	-o yaml | kubectl --context $(PROFILE) apply -f -
	@kubectl apply -f https://github.com/cert-manager/cert-manager/releases/download/v1.16.2/cert-manager.crds.yaml

cluster-ska-tango-operator: cluster-setup-ska-tango-operator ## install ska-tango-operator in the cluster
	@helm repo add skatelescope https://artefact.skao.int/repository/helm-internal
	@helm repo update skatelescope
	helm --kube-context $(PROFILE) --namespace=$(SKA_TANGO_OPERATOR_NAMESPACE) upgrade --install --wait --create-namespace ska-tango-operator $(SKA_TANGO_OPERATOR_CHART) $(SKA_TANGO_OPERATOR_PARAMS)

cluster-kyverno: ## install Kyverno in the cluster
	@helm repo add kyverno https://kyverno.github.io/kyverno/
	@helm repo update kyverno
	helm --kube-context $(PROFILE) --namespace=$(KYVERNO_NAMESPACE) upgrade --install --wait --create-namespace kyverno kyverno/kyverno
	kubectl --context $(PROFILE) apply -f resources/kyverno-ingress-annotation-deprecation.yml

cluster-kubeconfig: ## print KUBECONFIG for Minikube and swapping to external IP (try: make cluster-kubeconfig > minikube_config; export KUBECONFIG=$(pwd)/minikube_config)
	@kubectl --context $(PROFILE) config view --raw --flatten --minify 1>/dev/null 2>/dev/null; \
	res=$$?; \
	if [[ $$res -eq 0 ]]; then \
		CLUSTER_IP=$$($(EXE_DIR)/minikube --profile $(PROFILE) ip 2>/dev/null) && \
		kubectl --context $(PROFILE) config view --raw --flatten --minify | sed "s/$${CLUSTER_IP}/$(IPADDR)/"; \
	else \
		echo "kubectl --context $(PROFILE) current-context missing - perhaps you need to make minikube-clean && make all ?"; \
	fi

cluster-test: ## 🔎 run an interactive  test deployment
	export CLASS=nginx; \
	bash ./scripts/test-ingress.sh

minikube-load-images: ## load images from the charts listed in K8S_CHARTS
	export K8S_CHART_PARAMS=$(K8S_CHART_PARAMS) EXE_DIR=$(EXE_DIR); \
	bash ./scripts/load-images.sh "$(K8S_CHARTS)"

cluster-cache: ## docker image caching proxy
ifeq ($(OS_NAME),darwin)
	@echo "MacOS not supported for the cache unfortunately"
else
	@$(MUST_SUDO) $(BIN_DIR)/docker rm -f $(CACHE_NAME) 1>/dev/null 2>&1 || true
	@$(MUST_SUDO) $(BIN_DIR)/podman rm -f $(CACHE_NAME) 1>/dev/null 2>&1 || true
	@if [ -n "$(strip $(USE_CACHE))" ]; then \
	mkdir -p $(CACHE_DATA)/docker_mirror_cache $(CACHE_DATA)/docker_mirror_certs; \
	$(MUST_SUDO) $(BIN_DIR)/$(DRIVER) rm -f $(CACHE_NAME) 1>/dev/null 2>/dev/null || true; \
	echo "Starting a new $(CACHE_NAME) in $(CACHE_DATA) ..."; \
	$(MUST_SUDO) $(BIN_DIR)/$(DRIVER) run --name $(CACHE_NAME) \
		-p 3128:3128 -e ENABLE_MANIFEST_CACHE=true \
		-v $(CACHE_DATA)/docker_mirror_cache:/docker_mirror_cache \
		-v $(CACHE_DATA)/docker_mirror_certs:/ca \
		-e REGISTRIES="$(REGISTRIES)" \
		-d docker.io/rpardini/docker-registry-proxy:$(CACHE_VERSION); \
	sleep 3; \
	fi
endif

cluster-registry: minikube-reset-docker-env ## Setup a local Docker Registry integrated with Minikube
	@echo "Installing Docker Registry to integrate with Minikube"
	@$(MUST_SUDO) mkdir -p $(CACHE_DATA)/docker_registry_cache
ifeq ($(DRIVER),kvm2)
	@$(MUST_SUDO) $(BIN_DIR)/docker rm -f $(REGISTRY_NAME) 2>/dev/null || true
	@$(MUST_SUDO) $(BIN_DIR)/docker run --name $(REGISTRY_NAME) \
		-p 5000:5000 \
		-v $(CACHE_DATA)/docker_registry_cache:/var/lib/registry \
		-d docker.io/library/registry:2
else
	@$(MUST_SUDO) $(BIN_DIR)/$(DRIVER) rm -f $(REGISTRY_NAME) 2>/dev/null || true
	@$(MUST_SUDO) $(BIN_DIR)/$(DRIVER) run --name $(REGISTRY_NAME) \
		-p 5000:5000 \
		-v $(CACHE_DATA)/docker_registry_cache:/var/lib/registry \
		-d docker.io/library/registry:2
endif

bats-reinstall: bats-uninstall bats-install ## reinstall bats-core dependencies

bats-uninstall:
	@rm -rf $(CURDIR)/scripts/bats-*

bats-install:
	@if [ -d $(CURDIR)/scripts/bats-core ]; then \
		echo "Skipping install as bats-core already exists"; \
	else \
		git clone --branch v1.4.1 https://github.com/bats-core/bats-core $(CURDIR)/scripts/bats-core; \
		git clone --branch v0.3.0 https://github.com/bats-core/bats-support $(CURDIR)/scripts/bats-support; \
		git clone --branch v2.0.0 https://github.com/bats-core/bats-assert $(CURDIR)/scripts/bats-assert; \
	fi

bats-test: bats-install ## Run unit tests using BATS
	@echo "The CI_JOB_ID=$(CI_JOB_ID)"
	rm -rf $(CURDIR)/build
	mkdir -p $(CURDIR)/build
	@cd $(CURDIR)/build && \
	echo "running tests in: $$(pwd) ..." && \
	export CI_JOB_ID=$(CI_JOB_ID) && \
	$(CURDIR)/scripts/bats-core/bin/bats \
		--jobs 1 \
		--report-formatter junit \
		-o $(CURDIR)/build $(CURDIR)/tests/unit


##########################################################
# START OF HAProxy config
# essentially create a heredoc of the HAProxy config file
# CAUTION!: variables get interpolated immediately
##########################################################
define HAPROXY_CONFIG
#############################
#  api-proxy config
#############################
frontend front-api-proxy
  bind *:6443
  mode tcp
  option tcplog
  option forwardfor
  default_backend back-api-proxy
  timeout client 1800s
  timeout client-fin 50s

backend back-api-proxy
  mode tcp
  option tcplog
  balance roundrobin
  option ssl-hello-chk
  option log-health-checks
  default-server inter 10s fall 2
  server server1 ${CLUSTER_IP}:6443 check
  timeout connect 10s
  timeout server 1800s

#############################
#  nginx-proxy config
#############################
frontend front-nginx-http-proxy
  bind *:80
  mode http
  option httplog
  default_backend back-nginx-http-proxy
  timeout client 1800s
  timeout client-fin 50s

backend back-nginx-http-proxy
  mode http
  option http-server-close
  option dontlognull
  balance roundrobin
  server server1 ${CLUSTER_IP}:80 check
  timeout connect 10s
  timeout server 1800s

frontend front-nginx-https-proxy
  bind *:443
  mode tcp
  option tcplog
  option forwardfor
  default_backend back-nginx-https-proxy
  timeout client 1800s
  timeout client-fin 50s

backend back-nginx-https-proxy
  mode tcp
  option tcplog
  balance roundrobin
  server server1 ${CLUSTER_IP}:443 check
  timeout connect 10s
  timeout server 1800s

endef

define ADD_HAPROXY_CONFIG
#############################
### Proxy for XPORTX
#############################
frontend front-XPORTX-https-proxy
  bind *:XPORTX
  mode tcp
  option tcplog
  option forwardfor
  default_backend back-XPORTX-https-proxy
  timeout client 1800s
  timeout client-fin 50s

backend back-XPORTX-https-proxy
  mode tcp
  option tcplog
  balance roundrobin
  server server1 ${CLUSTER_IP}:XPORTX check
  timeout connect 10s
  timeout server 1800s

endef

##########################################################
# END OF HAProxy config
##########################################################

define ADD_METALLB_CONFIG
apiVersion: metallb.io/v1beta1
kind: IPAddressPool
metadata:
  name: development
  namespace: metallb-system
spec:
  addresses:
  - ${IP_PREFIX}.95-${IP_PREFIX}.195
---
apiVersion: metallb.io/v1beta1
kind: L2Advertisement
metadata:
  name: l2-advert
  namespace: metallb-system
spec:
  ipAddressPools:
  - development

endef


define COREDNS_EXTERNAL_CONFIG
image:
  repository: coredns/coredns
  tag: "1.11.1"
  pullPolicy: IfNotPresent
replicaCount: 1
resources:
  limits:
    cpu: 100m
    memory: 128Mi
  requests:
    cpu: 100m
    memory: 128Mi
rollingUpdate:
  maxUnavailable: 1
  maxSurge: 25%
terminationGracePeriodSeconds: 30
podAnnotations: {}
serviceType: "LoadBalancer"
prometheus:
  service:
    enabled: false
    annotations:
      prometheus.io/scrape: "true"
      prometheus.io/port: "9153"
  monitor:
    enabled: false
    additionalLabels: {}
    namespace: ""
    interval: ""
service:
  name: ""
  annotations: {}
serviceAccount:
  create: true
  name: ""
  annotations: {}
rbac:
  create: true
  pspEnable: false
isClusterService: false
priorityClassName: ""
servers:
- zones:
  - zone: .
    scheme: "dns://"
    use_tcp: false
  port: 53
  plugins:
  - name: errors
  - name: health
    configBlock: |-
      lameduck 5s
  - name: ready
  - name: kubernetes
    parameters: $(CLUSTER_DOMAIN) in-addr.arpa ip6.arpa
    configBlock: |-
      pods insecure
      fallthrough in-addr.arpa ip6.arpa
      ttl 30
  - name: k8s_external
    parameters: svc.$(CLUSTER_DOMAIN)
  - name: cache
    parameters: 30
  - name: reload
  - name: loadbalance
extraConfig: {}
livenessProbe:
  enabled: true
  initialDelaySeconds: 60
  periodSeconds: 10
  timeoutSeconds: 5
  failureThreshold: 5
  successThreshold: 1
readinessProbe:
  enabled: true
  initialDelaySeconds: 30
  periodSeconds: 10
  timeoutSeconds: 5
  failureThreshold: 5
  successThreshold: 1
affinity: {}
nodeSelector: {}
tolerations: []
podDisruptionBudget: {}
zoneFiles: []
extraVolumes: []
extraVolumeMounts: []
extraSecrets: []
customLabels: {}
customAnnotations: {}
hpa:
  enabled: false
  minReplicas: 1
  maxReplicas: 2
  metrics: {}
autoscaler:
  enabled: false
  coresPerReplica: 256
  nodesPerReplica: 16
  min: 0
  max: 0
  includeUnschedulableNodes: false
  preventSinglePointFailure: true
  image:
    repository: k8s.gcr.io/cluster-proportional-autoscaler
    tag: "1.8.5"
    pullPolicy: IfNotPresent
  priorityClassName: ""
  affinity: {}
  nodeSelector: {}
  tolerations: []
  resources:
    requests:
      cpu: "20m"
      memory: "10Mi"
    limits:
      cpu: "20m"
      memory: "10Mi"
  configmap:
    annotations: {}
  livenessProbe:
    enabled: true
    initialDelaySeconds: 10
    periodSeconds: 5
    timeoutSeconds: 5
    failureThreshold: 3
    successThreshold: 1
deployment:
  enabled: true
  name: ""
  annotations: {}

endef

define SKA_TANGO_OPERATOR_CSR_CONF
[req]
req_extensions = v3_req
distinguished_name = req_distinguished_name
[req_distinguished_name]
[ v3_req ]
basicConstraints = CA:FALSE
keyUsage = nonRepudiation, digitalSignature, keyEncipherment
extendedKeyUsage = serverAuth
subjectAltName = @alt_names
[alt_names]
DNS.1 = ${SKA_TANGO_OPERATOR_PING_SERVICE}
DNS.2 = ${SKA_TANGO_OPERATOR_PING_SERVICE}.${SKA_TANGO_OPERATOR_NAMESPACE}
DNS.3 = ${SKA_TANGO_OPERATOR_PING_SERVICE}.${SKA_TANGO_OPERATOR_NAMESPACE}.svc

endef

define SKA_TANGO_OPERATOR_SIGN_REQUEST
apiVersion: certificates.k8s.io/v1
kind: CertificateSigningRequest
metadata:
  name: ${SKA_TANGO_OPERATOR_PING_SERVICE}-csr
spec:
  signerName: kubernetes.io/kube-apiserver-client
  request: $$SKA_TANGO_OPERATOR_CSR_B64
  usages:
  - digital signature
  - key encipherment
  - server auth

endef

export HAPROXY_CONFIG
export ADD_HAPROXY_CONFIG
export ADD_METALLB_CONFIG
export COREDNS_EXTERNAL_CONFIG
export SKA_TANGO_OPERATOR_CSR_CONF
export SKA_TANGO_OPERATOR_SIGN_REQUEST
