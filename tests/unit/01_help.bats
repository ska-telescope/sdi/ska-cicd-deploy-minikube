#!/usr/bin/env bats

# Load a library from the `${BATS_TEST_DIRNAME}/test_helper' directory.
#
# Globals:
#   none
# Arguments:
#   $1 - name of library to load
# Returns:
#   0 - on success
#   1 - otherwise
load_lib() {
  local name="$1"
  load "../../scripts/${name}/load"
}

load_lib bats-support
load_lib bats-assert

setup() {
    echo "Start setup - set path"
    cd ..
    echo "End setup"
}

teardown() {
    echo "IN teardown"
}


@test "HELP: run localhelp" {
    run make minikube-localhelp
    echo "$output"
    echo "status: $status"
    [ $status -eq 0 ]
}

@test 'HELP: check help output contains all|setup|minikube|load-images' {
  local -r log_txt="${BATS_TEST_TMPDIR}/log.txt"
  {
    make minikube-localhelp | tr -cd '\11\12\15\40-\176' | sed 's/\[36m//g' | sed 's/\[0m//g'
  } > "${log_txt}"
  run bash -c "egrep '^(all |setup|minikube|cluster|load-images)' ${log_txt} | wc -l | xargs"
  echo "Output: ${output}#"
  cat "${log_txt}"
  [ "$output" -ge "18" ]
}
