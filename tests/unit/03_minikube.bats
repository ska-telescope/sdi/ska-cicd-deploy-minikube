#!/usr/bin/env bats

# Load a library from the `${BATS_TEST_DIRNAME}/test_helper' directory.
#
# Globals:
#   none
# Arguments:
#   $1 - name of library to load
# Returns:
#   0 - on success
#   1 - otherwise
load_lib() {
  local name="$1"
  load "../../scripts/${name}/load"
}

load_lib bats-support
load_lib bats-assert

setup() {
    echo "Start setup - set path and do make setup"
    cd ..
    run make cluster-setup
    echo "$output"
    echo "End setup"
}

teardown() {
    echo "IN teardown"
}

@test "MINIKUBE: startup minikube on docker driver" {
    if [[ "${CI_JOB_ID}" != "local" ]]; then
      skip "Skipping - running in CI"
    fi
    run make minikube-install DRIVER=docker 3>&-
    echo "In the test CI_JOB_ID=${CI_JOB_ID}"
    echo "$output"
    echo "status: $status"
    [ $status -eq 0 ]
}

@test "MINIKUBE: run basic test - create resources and curl against docker driver" {
    if [[ "${CI_JOB_ID}" != "local" ]]; then
      skip "Skipping - running in CI"
    fi
    run make cluster-test 3>&-
    echo "$output"
    echo "status: $status"
    [ $status -eq 0 ]
}

@test "MINIKUBE: run clean on docker driver" {
    if [[ "${CI_JOB_ID}" != "local" ]]; then
      skip "Skipping - running in CI"
    fi
    run make clean DRIVER=docker 3>&-
    echo "$output"
    echo "status: $status"
    [ $status -eq 0 ]
}

@test "MINIKUBE: startup minikube on podman driver" {
    if [[ "${CI_JOB_ID}" != "local" ]]; then
      skip "Skipping - running in CI"
    fi
    run make minikube-install DRIVER=podman 3>&-
    echo "$output"
    echo "status: $status"
    [ $status -eq 0 ]
}

@test "MINIKUBE: run basic test - create resources and curl against podman driver" {
    if [[ "${CI_JOB_ID}" != "local" ]]; then
      skip "Skipping - running in CI"
    fi
    run make cluster-test 3>&-
    echo "$output"
    echo "status: $status"
    [ $status -eq 0 ]
}

@test "MINIKUBE: run clean on podman driver" {
    if [[ "${CI_JOB_ID}" != "local" ]]; then
      skip "Skipping - running in CI"
    fi
    run make clean DRIVER=podman 3>&-
    echo "$output"
    echo "status: $status"
    [ $status -eq 0 ]
}
