#!/usr/bin/env bats

# Load a library from the `${BATS_TEST_DIRNAME}/test_helper' directory.
#
# Globals:
#   none
# Arguments:
#   $1 - name of library to load
# Returns:
#   0 - on success
#   1 - otherwise
load_lib() {
  local name="$1"
  load "../../scripts/${name}/load"
}

load_lib bats-support
load_lib bats-assert

setup() {
    echo "Start setup - set path and do make setup"
    cd ..
    run make cluster-setup vault-setup
    echo "$output"
    echo "End setup"
}

teardown() {
    echo "IN teardown - nothing to do"
}

@test "SETUP: check minikube exists" {
    run /usr/local/bin/minikube version --short
    echo "Version: $output"
    [ $status -eq 0 ]
}

@test "SETUP: check kubectl exists" {
    run /usr/local/bin/kubectl  version --client
    echo "Version: $output"
    [ $status -eq 0 ]
}

@test "SETUP: check helm exists" {
    run /usr/local/bin/helm version --short
    echo "Version: $output"
    [ $status -eq 0 ]
}

@test "SETUP: check k9s exists" {
    run /usr/local/bin/k9s version --short
    echo "Version: $output"
    [ $status -eq 0 ]
}

@test "SETUP: check yq exists" {
    run /usr/local/bin/yq --version
    echo "Version: $output"
    [ $status -eq 0 ]
}

@test "SETUP: check helmfile exists" {
    run /usr/local/bin/helmfile --version
    echo "Version: $output"
    [ $status -eq 0 ]
}

@test "SETUP: check kyverno exists" {
    run /usr/local/bin/kyverno version
    echo "Version: $output"
    [ $status -eq 0 ]
}

@test "SETUP: check vault exists" {
    run /usr/local/bin/vault version
    echo "Version: $output"
    [ $status -eq 0 ]
}

@test "SETUP: check envconsul exists" {
    run /usr/local/bin/envconsul --version
    echo "Version: $output"
    [ $status -eq 0 ]
}

@test "SETUP: check consul-template exists" {
    run /usr/local/bin/consul-template --version
    echo "Version: $output"
    [ $status -eq 0 ]
}
