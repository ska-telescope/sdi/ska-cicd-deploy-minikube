PROFILE ?= minikube ## The Minikube profile to boot with
VAULT_VERSION ?= 1.18.1## Vault version to install https://releases.hashicorp.com/vault/
CONSUL_TEMPLATE_VERSION ?= 0.39.1## consul-template version to install https://releases.hashicorp.com/consul-template
ENVCONSUL_VERSION ?= 0.13.2## consul-template version to install https://releases.hashicorp.com/envconsul/
MEDUSA_VERSION ?= 0.7.2## medusa version to install https://github.com/jonasvinther/medusa/releases/

VAULT_DEFAULT_AUTH ?= kubernetes## Vault auth method name
VAULT_DEFAULT_MOUNT ?= dev## default mount point
VAULT_DEFAULT_ROLE ?= kube-role## default Vault role
VAULT_SA ?= vault-auth## Service account Name for Vault
VAULT_NAMESPACE ?= vault## Vault namespace for installation
VAULT_AUTH_NAMESPACE ?= default## Namespace for Vault auth integration
VAULT_K8S_ISSUER ?= https://kubernetes.default.svc.cluster.local## Certificate issue and API endpoint
VAULT_ENGINES ?= kv dev mid-itf low-itf secret ## Vault kv engines to initialise
VAULT_MAX_VERSIONS ?= 10000 ## Default maximum number of versions to retain for a secret
VAULT_DEV_SERVER_TOKEN ?= root## Root auth token for dev Vault instance
VAULT_USER ?= $(USER)## vault.skao.int user account name for personal secrets
VAULT_EXPORT_PATH ?= /kv/users/$(VAULT_USER)## Export path from vault.skao.int - defaults to /kv/users/<VAULT_USER>
VAULT_PATH ?= kv/## Default Vault path
VAULT_ENGINE ?= kv-v2## Default Vault engine kv version 2
ifeq ($(VAULT_ENGINE),kv)
MEDUSA_KEY_TYPE := -m kv1
endif
VAULT_MOUNT ?= dev## Vault mount for kvput
VAULT_KVPATH ?= config## Vault path added to VAULT_MOUNT for kvput
VAULT_FIELD ?= testvalue## Vault fieldname for kvput
VAULT_VALUE ?= $(VAULT_FIELD)=Wahoooo## Vault fieldname=value for kvput
VAULT_ADDRESS ?= http://$(shell $(EXE_DIR)/kubectl --context $(PROFILE) get svc -n $(VAULT_NAMESPACE) vault -o jsonpath='{.status.loadBalancer.ingress[0].ip}' 2>/dev/null):8200
VAULT_CSI_ENABLED ?=false## Vault CSI Driver enabled
VAULT_INJECTOR_ENABLED ?=false## Vault Injector enabled

vault-deploy: ## Deploy integrated Vault server with injector and CSI support
	@helm repo add hashicorp https://helm.releases.hashicorp.com
	@helm upgrade --install vault hashicorp/vault \
		--set "server.dev.enabled=true" \
		--set "server.service.type=LoadBalancer" \
		--set "ui.enabled=true" \
		--set "ui.serviceType=LoadBalancer" \
		--set "csi.enabled=$(VAULT_CSI_ENABLED)" \
		--set "csi.agent.enabled=$(VAULT_CSI_ENABLED)" \
		--set "injector.enabled=$(VAULT_INJECTOR_ENABLED)" \
		--set "injector.authPath=auth/$(VAULT_DEFAULT_AUTH)" \
		--namespace $(VAULT_NAMESPACE) \
		--create-namespace

ifeq ($(VAULT_CSI_ENABLED),true)
	@helm repo add secrets-store-csi-driver \
		https://kubernetes-sigs.github.io/secrets-store-csi-driver/charts
	@helm upgrade --install csi-secrets-store secrets-store-csi-driver/secrets-store-csi-driver \
		--namespace kube-system \
		--set syncSecret.enabled=true
endif

	# wait for the first Pod of the Vault StatefulSet
	@kubectl -n $(VAULT_NAMESPACE) wait pod/vault-0 --for condition=ready --timeout=180s

vault-deploy-secrets-operator: ## Deploy Vault secrets operator
	@helm repo add hashicorp https://helm.releases.hashicorp.com
	@echo "$${VAULT_VSO_VALUES}" > /tmp/vault-vso-values.yml
	@helm upgrade --install vault-secrets-operator hashicorp/vault-secrets-operator \
		--namespace $(VAULT_NAMESPACE) \
		--create-namespace \
		-f /tmp/vault-vso-values.yml
	@rm -f /tmp/vault-vso-values.yml

VAULT_OPERATOR_VALUES_FILE ?= vault-operator-values.yaml
vault-operator: ## Deploy Vault Operator
	@helm repo add hashicorp https://helm.releases.hashicorp.com
	@echo "$${VAULT_OPERATOR_VALUES}" > /tmp/$(VAULT_OPERATOR_VALUES_FILE)

	@helm upgrade --install vault-secrets-operator hashicorp/vault-secrets-operator \
		--values /tmp/$(VAULT_OPERATOR_VALUES_FILE) \
		--namespace $(VAULT_NAMESPACE) --create-namespace

	@rm -f /tmp/$(VAULT_OPERATOR_VALUES_FILE)

	# wait for the first Pod of the Vault Operator Deployment
	@kubectl -n ${VAULT_NAMESPACE} wait --for=condition=available \
	  deployment.v1.apps/vault-secrets-operator-controller-manager \
	  --timeout=180s

VAULT_OPERATOR_STATIC_EXAMPLE_FILE ?= vault-operator-static-example.yaml
vault-operator-static-example: # create an auth and a static secret example
	@echo "$${VAULT_OPERATOR_STATIC_EXAMPLE}" > /tmp/$(VAULT_OPERATOR_STATIC_EXAMPLE_FILE)
	@kubectl apply -f /tmp/$(VAULT_OPERATOR_STATIC_EXAMPLE_FILE)
	@rm -f /tmp/$(VAULT_OPERATOR_STATIC_EXAMPLE_FILE)

vault-engines: ## Setup a default set of Vault engines
	@for i in $(VAULT_ENGINES); do \
	EXISTS=`VAULT_TOKEN=$(VAULT_DEV_SERVER_TOKEN) $(EXE_DIR)/vault secrets list -address=$(VAULT_ADDRESS) | grep "$${i}/ " 2>/dev/null`; \
	if [ -z "$${EXISTS}" ]; then \
		VAULT_TOKEN=$(VAULT_DEV_SERVER_TOKEN) $(EXE_DIR)/vault secrets enable -address=$(VAULT_ADDRESS) -path=$${i} $(VAULT_ENGINE); \
		VAULT_TOKEN=$(VAULT_DEV_SERVER_TOKEN) $(EXE_DIR)/vault write -address=$(VAULT_ADDRESS) "$${i}/config" max_versions=$(VAULT_MAX_VERSIONS); \
	fi; \
	done

	@make vault-kvput VAULT_MOUNT=$(VAULT_DEFAULT_MOUNT) VAULT_KVPATH=za-gateway/seed \
		VAULT_VALUE="pxeboot_file=myfancy.bin default_driver=idrac"

vault-k8s-integration: ## Integrate Vault with Kubernetes
	@EXISTS=`VAULT_TOKEN=$(VAULT_DEV_SERVER_TOKEN) $(EXE_DIR)/vault auth list -address=$(VAULT_ADDRESS) | grep "$(VAULT_DEFAULT_AUTH)/ " 2>/dev/null`; \
	if [ -z "$${EXISTS}" ]; then \
		VAULT_TOKEN=$(VAULT_DEV_SERVER_TOKEN) $(EXE_DIR)/vault auth enable -address=$(VAULT_ADDRESS) -path=$(VAULT_DEFAULT_AUTH) kubernetes; \
	fi

	@$(EXE_DIR)/kubectl --context $(PROFILE) get -n $(VAULT_AUTH_NAMESPACE) sa $(VAULT_SA) 1>/dev/null 2>/dev/null; \
	EXISTS=$$?; \
	if [ $${EXISTS} -eq 0 ]; then \
		$(EXE_DIR)/kubectl --context $(PROFILE) delete -n $(VAULT_AUTH_NAMESPACE) sa $(VAULT_SA); \
		sleep 1; \
	fi; \
	$(EXE_DIR)/kubectl --context $(PROFILE) create -n $(VAULT_AUTH_NAMESPACE) sa $(VAULT_SA)

	@echo "$${VAULT_K8S_SECRET}" > /tmp/vault-k8s-secret.yaml
	@$(EXE_DIR)/kubectl --context $(PROFILE) -n $(VAULT_AUTH_NAMESPACE) apply -f /tmp/vault-k8s-secret.yaml
	@rm -f /tmp/vault-k8s-secret.yaml

	@echo "$${VAULT_K8S_CLUSTER_ROLE_BINDING}" > /tmp/vault-k8s-cluster-role-binding.yaml
	@$(EXE_DIR)/kubectl --context $(PROFILE) apply -f /tmp/vault-k8s-cluster-role-binding.yaml
	@rm -f /tmp/vault-k8s-cluster-role-binding.yaml

	@K8S_TOKEN=`$(EXE_DIR)/kubectl --context $(PROFILE) get -n $(VAULT_AUTH_NAMESPACE) secret  $(VAULT_SA) -o json | jq -r .data.token | base64 -d`; \
	K8S_CA_CERT=`$(EXE_DIR)/kubectl --context $(PROFILE) get -n $(VAULT_AUTH_NAMESPACE) secret  $(VAULT_SA) -o json | jq -r '.data["ca.crt"]' | base64 -d`; \
	VAULT_TOKEN=$(VAULT_DEV_SERVER_TOKEN) $(EXE_DIR)/vault \
	write -address=$(VAULT_ADDRESS) auth/$(VAULT_DEFAULT_AUTH)/config \
    kubernetes_host="$(VAULT_K8S_ISSUER)" \
	token_reviewer_jwt="$${K8S_TOKEN}" \
	kubernetes_ca_cert="$${K8S_CA_CERT}" \
	disable_local_ca_jwt=true \
	issuer="$(VAULT_K8S_ISSUER)"

	@echo "$${VAULT_K8S_POLICY}" > /tmp/vault-k8s-policy.hcl
	@VAULT_TOKEN=$(VAULT_DEV_SERVER_TOKEN) $(EXE_DIR)/vault \
		policy write -address=$(VAULT_ADDRESS) \
		k8spolicy /tmp/vault-k8s-policy.hcl
	@rm -f /tmp/vault-k8s-policy.hcl

	@VAULT_TOKEN=$(VAULT_DEV_SERVER_TOKEN) $(EXE_DIR)/vault \
		write -address=$(VAULT_ADDRESS) auth/$(VAULT_DEFAULT_AUTH)/role/$(VAULT_DEFAULT_ROLE) \
		alias_name_source=serviceaccount_uid \
		bound_service_account_names="*" \
	    bound_service_account_namespaces="*" \
		token_policies=k8spolicy \
		token_ttl=24h \
        token_no_default_policy=false

vault-kvput: ## kvput Vault secrets
	VAULT_TOKEN=$(VAULT_DEV_SERVER_TOKEN) $(EXE_DIR)/vault \
	kv put -address=$(VAULT_ADDRESS) \
		-mount=$(VAULT_MOUNT) $(VAULT_KVPATH) $(VAULT_VALUE)

vault-port-forward: ## Port forward the Vault API to localhost:8200
	kubectl port-forward service/vault -n vault 8200:8200

vault-port-forward-stop: ## Kill and existing Vault port forward
	@kill `ps axf | grep port-forward | grep 8200 | grep -v grep | awk '{print $$1}'` 1>/dev/null 2>&1 || true

vault-ui: ## Launch the browser to the Vault UI
	minikube service -n vault vault-ui

vault-ui-url: ## Port-forward the Vault UI
	minikube service -n vault vault-ui --url

vault-cli-config: ## Export Vault configuration
	@echo "export VAULT_ADDR=$(VAULT_ADDRESS)"
	@echo "export VAULT_TOKEN=$(VAULT_DEV_SERVER_TOKEN)"

vault-setup: ## install commandline apps for Vault
	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/vault.zip)
	$(eval RIGHT_VAULT := $(shell $(EXE_DIR)/vault version 2>&1 | grep "Vault v$(VAULT_VERSION)"))
	@if [ ! -f "$(EXE_DIR)/vault" ] || [ -z "$(RIGHT_VAULT)" ]; then \
		echo "Installing vault version $(VAULT_VERSION)"; \
		curl -Lo $(TMP_FILE) https://releases.hashicorp.com/vault/$(VAULT_VERSION)/vault_$(VAULT_VERSION)_$(OS_NAME)_$(OS_BIN).zip \
		&& unzip -p $(TMP_FILE) vault > /tmp/vault && $(SUDO_FOR_EXE_DIR) mv -f /tmp/vault $(EXE_DIR)/vault  \
		&& $(SUDO_FOR_EXE_DIR) chmod +x $(EXE_DIR)/vault; \
	else \
		echo "vault $(VAULT_VERSION) already installed"; \
	fi
	@rm -rf $(TEMPDIR)

	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/envconsul.zip)
	$(eval RIGHT_ENVCONSUL := $(shell $(EXE_DIR)/envconsul --version 2>&1 | grep "envconsul v$(ENVCONSUL_VERSION)"))
	@if [ ! -f "$(EXE_DIR)/envconsul" ] || [ -z "$(RIGHT_ENVCONSUL)" ]; then \
		echo "Installing envconsul version $(ENVCONSUL_VERSION)"; \
		curl -Lo $(TMP_FILE) https://releases.hashicorp.com/envconsul/$(ENVCONSUL_VERSION)/envconsul_$(ENVCONSUL_VERSION)_$(OS_NAME)_$(OS_BIN).zip \
		&& unzip -p $(TMP_FILE) envconsul > /tmp/envconsul && $(SUDO_FOR_EXE_DIR) mv -f /tmp/envconsul $(EXE_DIR)/envconsul  \
		&& $(SUDO_FOR_EXE_DIR) chmod +x $(EXE_DIR)/envconsul; \
	else \
		echo "envconsul $(ENVCONSUL_VERSION) already installed"; \
	fi
	@rm -rf $(TEMPDIR)

	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/consul-template.zip)
	$(eval RIGHT_CONSUL_TEMPLATE := $(shell $(EXE_DIR)/consul-template --version 2>&1 | grep "consul-template v$(CONSUL_TEMPLATE_VERSION)"))
	@if [ ! -f "$(EXE_DIR)/consul-template" ] || [ -z "$(RIGHT_CONSUL_TEMPLATE)" ]; then \
		echo "Installing consul-template version $(CONSUL_TEMPLATE_VERSION)"; \
		curl -Lo $(TMP_FILE) https://releases.hashicorp.com/consul-template/$(CONSUL_TEMPLATE_VERSION)/consul-template_$(CONSUL_TEMPLATE_VERSION)_$(OS_NAME)_$(OS_BIN).zip \
		&& unzip -p $(TMP_FILE) consul-template > /tmp/consul-template && $(SUDO_FOR_EXE_DIR) mv -f /tmp/consul-template $(EXE_DIR)/consul-template  \
		&& $(SUDO_FOR_EXE_DIR) chmod +x $(EXE_DIR)/consul-template; \
	else \
		echo "consul-template $(CONSUL_TEMPLATE_VERSION) already installed"; \
	fi
	@rm -rf $(TEMPDIR)

	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/medusa.tar.gz)
	$(eval RIGHT_MEDUSA := $(shell $(EXE_DIR)/medusa version 2>&1 | grep "medusa v$(MEDUSA_VERSION)"))
	@if [ ! -f "$(EXE_DIR)/medusa" ] || [ -z '$(RIGHT_MEDUSA)' ]; then \
		echo "Installing medusa version $(MEDUSA_VERSION)"; \
		curl -Lo $(TMP_FILE) https://github.com/jonasvinther/medusa/releases/download/v$(MEDUSA_VERSION)/medusa_$(MEDUSA_VERSION)_$(OS_NAME)_$(OS_BIN).tar.gz \
		&& tar -xzOf $(TMP_FILE) medusa > /tmp/medusa && $(SUDO_FOR_EXE_DIR) mv -f /tmp/medusa $(EXE_DIR)/medusa  \
		&& $(SUDO_FOR_EXE_DIR) chmod +x $(EXE_DIR)/medusa; \
	else \
		echo "medusa $(MEDUSA_VERSION) already installed"; \
	fi
	@rm -rf $(TEMPDIR)

vault-export: ## Export a given VAULT_EXPORT_PATH from vault.skao.int
	@echo "Enter vault token from https://vault.skao.int: "; \
	read EXP_TOKEN ; \
	VAULT_TOKEN=$$EXP_TOKEN medusa \
	--address https://vault.skao.int \
	export $(VAULT_EXPORT_PATH)

vault-import: ## Export a given VAULT_EXPORT_PATH from vault.skao.int and import to dev server at VAULT_PATH
	@VAULT_TOKEN=$(VAULT_DEV_SERVER_TOKEN) $(EXE_DIR)/vault secrets list -address=$(VAULT_ADDRESS) -format=json | \
	jq "keys" | \
	grep "$(VAULT_PATH)" 1>/dev/null 2>&1 || \
	VAULT_TOKEN=$(VAULT_DEV_SERVER_TOKEN) $(EXE_DIR)/vault secrets enable \
	-address=$(VAULT_ADDRESS) \
	-path=$(VAULT_PATH) $(VAULT_ENGINE) || true

	@echo "Enter VAULT_TOKEN for https://vault.skao.int: "; \
	read EXP_TOKEN ; \
	VAULT_TOKEN=$$EXP_TOKEN medusa \
	--address https://vault.skao.int \
	export $(VAULT_EXPORT_PATH) | tee | \
	VAULT_TOKEN=$(VAULT_DEV_SERVER_TOKEN) medusa \
	import /$(VAULT_PATH) - --address=$(VAULT_ADDRESS) $(MEDUSA_KEY_TYPE)  || true

VAULT_CONFIG_FILE ?= config.yml
vault-config-template: ## Example of consul-template integrated with vault
	rm -f $(VAULT_CONFIG_FILE)
	@echo "$${TEMPLATE_CONFIG}" > /tmp/$(VAULT_CONFIG_FILE).tpl
	export KV_TOKEN=$$(VAULT_TOKEN=$(VAULT_DEV_SERVER_TOKEN) $(EXE_DIR)/vault \
		token create -address=$(VAULT_ADDRESS) -policy="k8spolicy" \
		-format json | jq -r '.auth | .client_token'); \
	echo "KV_TOKEN: $$KV_TOKEN "; \
	VAULT_TOKEN=$$KV_TOKEN consul-template \
		-vault-addr=$(VAULT_ADDRESS) -template="/tmp/$(VAULT_CONFIG_FILE).tpl:$(VAULT_CONFIG_FILE)" -once
	ls -latr $(VAULT_CONFIG_FILE)
	cat $(VAULT_CONFIG_FILE)
	rm -f $(VAULT_CONFIG_FILE)

APP_SH ?= app.sh
vault-envconsul-test: ## Example of envconsul integrated with vault
	@echo "$${ENVCONSUL_APP}" > /tmp/$(APP_SH)
	@chmod a+x /tmp/$(APP_SH)

	@export KV_TOKEN=$$(VAULT_TOKEN=$(VAULT_DEV_SERVER_TOKEN) $(EXE_DIR)/vault \
		token create -address=$(VAULT_ADDRESS) -policy="k8spolicy" \
		-format json | jq -r '.auth | .client_token'); \
	echo "KV_TOKEN: $$KV_TOKEN "; \
	VAULT_TOKEN=$$KV_TOKEN envconsul \
		-vault-addr=$(VAULT_ADDRESS) \
		-upcase -secret dev/za-gateway/seed /tmp/$(APP_SH)
	@rm -f /tmp/$(APP_SH)

vault-example-apps: ## Example of Vault injector and CSI applications
	@kubectl apply -f resources/vault-injector-test-minikube.yml
	@kubectl apply -f resources/vault-csi-driver-test-minikube.yml
	@echo "Waithing for Pods to start..."
	@kubectl wait --for=condition=ready pod --all --timeout=180s -n default
	@echo "";  echo "Running pods:"
	@kubectl get pods
	@echo ""
	@echo "Show CSI secrets:"
	kubectl -n default exec vault-test-csi-pod -c app -- printenv
	@echo ""; echo "Show Injector secrets:"
	kubectl -n default exec vault-test-injector -c app -- cat /vault/secrets/config.yaml
	@echo ""; echo "Cleanup..."
	@kubectl delete -f resources/vault-injector-test-minikube.yml
	@kubectl delete -f resources/vault-csi-driver-test-minikube.yml

vault-install: vault-setup vault-deploy vault-engines vault-k8s-integration vault-operator ## Full end to end Vault install and integration

define TEMPLATE_CONFIG
---
{{- with secret "dev/za-gateway/seed" }}
pxebootfile: "{{ .Data.data.pxeboot_file }}"
default_driver: "{{ .Data.data.default_driver }}"
{{- end }}

endef
export TEMPLATE_CONFIG

define ENVCONSUL_APP
#!/usr/bin/env bash

printenv | grep DEV
cat <<EOT
My seed info is:

pxebootfile: "$${DEV_ZA_GATEWAY_SEED_PXEBOOT_FILE}"
driver:      "$${DEV_ZA_GATEWAY_SEED_DEFAULT_DRIVER}"

EOT
endef
export ENVCONSUL_APP

define VAULT_K8S_SECRET
apiVersion: v1
kind: Secret
type: kubernetes.io/service-account-token
metadata:
  name: $(VAULT_SA)
  namespace: $(VAULT_AUTH_NAMESPACE)
  annotations:
    kubernetes.io/service-account.name: $(VAULT_SA)

endef
export VAULT_K8S_SECRET

define VAULT_K8S_CLUSTER_ROLE_BINDING
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: role-tokenreview-binding
  namespace: $(VAULT_AUTH_NAMESPACE)
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: system:auth-delegator
subjects:
  - kind: ServiceAccount
    name: $(VAULT_SA)
    namespace: $(VAULT_AUTH_NAMESPACE)

endef
export VAULT_K8S_CLUSTER_ROLE_BINDING

define VAULT_K8S_POLICY

path "secret/*" {
  capabilities = ["read", "list"]
}
path "kv/*" {
  capabilities = ["read", "list"]
}
path "shared/*" {
  capabilities = ["read", "list"]
}
path "mid-itf/*" {
  capabilities = ["read", "list"]
}
path "low-itf/*" {
  capabilities = ["read", "list"]
}
path "dev/*" {
  capabilities = ["read", "list"]
}

endef
export VAULT_K8S_POLICY

define VAULT_VSO_VALUES

defaultAuthMethod:
  enabled: true
  kubernetes:
    role: $(VAULT_DEFAULT_ROLE)
    serviceAccount: default
    tokenAudiences:
    - vault
  mount: $(VAULT_DEFAULT_AUTH)

defaultVaultConnection:
  address: $(VAULT_ADDRESS)
  enabled: true

endef
export VAULT_VSO_VALUES

define VAULT_OPERATOR_VALUES

defaultVaultConnection:
  enabled: true
  address: "http://vault.vault.svc.cluster.local:8200"
  skipTLSVerify: false
controller:
  manager:
    clientCache:
      persistenceModel: direct-encrypted
      storageEncryption:
        enabled: true
        mount: $(VAULT_DEFAULT_AUTH)
        keyName: vso-client-cache
        transitMount: dev
        kubernetes:
          role: $(VAULT_DEFAULT_ROLE)
          serviceAccount: default

endef
export VAULT_OPERATOR_VALUES

define VAULT_OPERATOR_STATIC_EXAMPLE

---
apiVersion: secrets.hashicorp.com/v1beta1
kind: VaultAuth
metadata:
  name: static-auth-example
  namespace: default
spec:
  method: kubernetes
  mount: kubernetes
  kubernetes:
    role: kube-role
    serviceAccount: default
    audiences:
      - vault

---
apiVersion: secrets.hashicorp.com/v1beta1
kind: VaultStaticSecret
metadata:
  name: vault-kv-app
  namespace: default
spec:
  type: kv-v2

  # mount path
  mount: dev

  # path of the secret
  path: za-gateway/seed

  # dest k8s secret
  destination:
    name: za-gateway-seed
    create: true

  # static secret refresh interval
  refreshAfter: 30s

  # Name of the CRD to authenticate to Vault
  vaultAuthRef: static-auth-example

endef
export VAULT_OPERATOR_STATIC_EXAMPLE
