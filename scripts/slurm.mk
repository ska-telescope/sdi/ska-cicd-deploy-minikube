SLURM_SKA_TANGO_OPERATOR_ENABLED ?= false##decide whether or not to deploy tango operator for Slurm

# https://github.com/SlinkyProject/slurm-operator/blob/main/docs/user/quickstart.md
SLURM_OPERATOR_VERSION ?= 0.1.0
slurm-all: ## Build all of prometheus and slurm integration from scratch
	make clean PROFILE=minikube
	make all PROFILE=minikube \
	 NODES=1 \
	 CILIUM=yes \
	 KYVERNO=no \
	 TOOLS=yes \
	 SKA_TANGO_OPERATOR_ENABLED=$(SLURM_SKA_TANGO_OPERATOR_ENABLED)
	make slurm-prometheus
	make slurm
	make slurm-cluster

slurm: ## Install slurm
	curl -L https://raw.githubusercontent.com/SlinkyProject/slurm-operator/refs/tags/v$(SLURM_OPERATOR_VERSION)/helm/slurm-operator/values.yaml \
	-o values-slurm-operator.yaml
	helm --kube-context $(PROFILE) upgrade --install slurm-operator oci://ghcr.io/slinkyproject/charts/slurm-operator \
	--values=values-slurm-operator.yaml \
	--version=$(SLURM_OPERATOR_VERSION) \
	--namespace=slinky \
	--create-namespace
	@sleep 3
	kubectl --context $(PROFILE) -n slinky rollout status deployment slurm-operator --timeout=5m
	kubectl --context $(PROFILE) -n slinky rollout status deployment slurm-operator-webhook --timeout=5m

slurm-cluster: ## Create a slurm cluster
	curl -L https://raw.githubusercontent.com/SlinkyProject/slurm-operator/refs/tags/v$(SLURM_OPERATOR_VERSION)/helm/slurm/values.yaml \
	-o values-slurm-cluster.yaml
	helm --kube-context $(PROFILE) upgrade --install slurm oci://ghcr.io/slinkyproject/charts/slurm \
	--values=values-slurm-cluster.yaml \
	--version=$(SLURM_OPERATOR_VERSION) \
	--namespace=slurm \
	--create-namespace \
	--set compute.nodesets[0].replicas=3
	@sleep 5
	# missing correct label
	kubectl --context $(PROFILE) -n slurm label --overwrite servicemonitor slurm-exporter release=prometheus

slurm-prometheus: ## Install prometheus for slurm
	helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
	helm repo add metrics-server https://kubernetes-sigs.github.io/metrics-server/
	helm repo add bitnami https://charts.bitnami.com/bitnami
	helm repo add jetstack https://charts.jetstack.io
	helm repo update prometheus-community metrics-server bitnami jetstack
	helm --kube-context $(PROFILE) upgrade --install cert-manager jetstack/cert-manager \
		--namespace cert-manager --create-namespace --set crds.enabled=true
	@sleep 3
	kubectl --context $(PROFILE) -n cert-manager rollout status deployment cert-manager --timeout=5m
	helm --kube-context $(PROFILE) upgrade --install prometheus prometheus-community/kube-prometheus-stack \
		--namespace prometheus \
		--create-namespace \
		--set installCRDs=true \
		--set grafana.ingress.enabled=true \
		--set alertmanager.service.type=LoadBalancer \
		--set prometheus.service.type=LoadBalancer
	@sleep 3
	kubectl --context $(PROFILE) -n prometheus rollout status deployment prometheus-kube-prometheus-operator --timeout=5m
	@sleep 10
	kubectl --context $(PROFILE) -n prometheus wait -l statefulset.kubernetes.io/pod-name=prometheus-prometheus-kube-prometheus-prometheus-0 --for=condition=ready pod --timeout=5m
	kubectl --context $(PROFILE) -n prometheus patch svc prometheus-grafana -p '{"spec": {"type": "LoadBalancer"}}'
	@PROM_USER=`kubectl --context $(PROFILE) -n prometheus get secret prometheus-grafana -o jsonpath="{.data.admin-user}" | base64 --decode`; \
	echo "Grafana user: $${PROM_USER}"
	@PROM_PASSWORD=`kubectl --context $(PROFILE) -n prometheus get secret prometheus-grafana -o jsonpath="{.data.admin-password}" | base64 --decode`; \
	echo "Grafana password: $${PROM_PASSWORD}"

slurm-test: ## Run a simple test for the slurm cluster
	echo 'sinfo; srun hostname; sbatch --wrap="sleep 60"; squeue' | \
	kubectl --namespace=slurm exec -i statefulsets/slurm-controller -- bash --login

slurm-shell: ## Open a shell to the slurm cluster login node
	kubectl --namespace=slurm exec -it statefulsets/slurm-controller -- bash --login
