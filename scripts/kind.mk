
kind-ip: ## extract IP address for Kind controlplane
	$(eval CLUSTER_IP := $(shell docker network inspect kind | jq -r '.[0].Containers[] | select(.Name=="skao-control-plane") | .IPv4Address' | cut -d '/' -f 1))

kind-metallb-config: kind-ip metallb-config ## Kind Metallb configuration

kind-extdns: kind-ip config-extdns ## Deploy CoreDNS k8s_external responder

kind-update-dns: kind-ip update-dns ## Update DNS when using Kind

kind-clean-dns: kind-ip clean-dns ## Clean DNS when using Kind

# containerdConfigPatches:
# - |-
#   [plugins."io.containerd.grpc.v1.cri".registry.mirrors."docker.io"]
#     endpoint = ["http://$(IPADDR):3128"]
#   [plugins."io.containerd.grpc.v1.cri".registry.configs."docker.io".tls]
#     insecure_skip_verify = true

define KIND_CONFIG
---
kind: Cluster
apiVersion: kind.x-k8s.io/v1alpha4
name: skao

containerdConfigPatches:
  - |-
    [plugins."io.containerd.grpc.v1.cri".registry.mirrors."docker.io"]
      endpoint = ["http://$(IPADDR):5000"]
    [plugins."io.containerd.grpc.v1.cri".registry.configs."docker.io".tls]
      insecure_skip_verify = true
    [plugins."io.containerd.grpc.v1.cri".registry.mirrors."registry-1.docker.io"]
      endpoint = ["http://$(IPADDR):5000"]
    [plugins."io.containerd.grpc.v1.cri".registry.configs."registry-1.docker.io".tls]
      insecure_skip_verify = true
    [plugins."io.containerd.grpc.v1.cri".registry.mirrors."quay.io"]
      endpoint = ["http://$(IPADDR):5000"]
    [plugins."io.containerd.grpc.v1.cri".registry.configs."quay.io".tls]
      insecure_skip_verify = true
    [plugins."io.containerd.grpc.v1.cri".registry.mirrors."gcr.io"]
      endpoint = ["http://$(IPADDR):5000"]
    [plugins."io.containerd.grpc.v1.cri".registry.configs."gcr.io".tls]
      insecure_skip_verify = true
    [plugins."io.containerd.grpc.v1.cri".registry.mirrors."registry.k8s.io"]
      endpoint = ["http://$(IPADDR):5000"]
    [plugins."io.containerd.grpc.v1.cri".registry.configs."registry.k8s.io".tls]
      insecure_skip_verify = true
    [plugins."io.containerd.grpc.v1.cri".registry.mirrors."artefact.skao.int"]
      endpoint = ["http://$(IPADDR):5000"]
    [plugins."io.containerd.grpc.v1.cri".registry.configs."artefact.skao.int".tls]
      insecure_skip_verify = true
    [plugins."io.containerd.grpc.v1.cri".registry.mirrors."registry.gitlab.com"]
      endpoint = ["http://$(IPADDR):5000"]
    [plugins."io.containerd.grpc.v1.cri".registry.configs."registry.gitlab.com".tls]
      insecure_skip_verify = true

nodes:
- role: control-plane
  image: kindest/node:$(KUBERNETES_VERSION)
  kubeadmConfigPatches:
  - |
    kind: InitConfiguration
    nodeRegistration:
      kubeletExtraArgs:
        node-labels: "ingress-ready=true"
  extraPortMappings:
  - containerPort: 80
    hostPort: 8081
    protocol: TCP
  - containerPort: 443
    hostPort: 8443
    protocol: TCP

- role: worker
  image: kindest/node:$(KUBERNETES_VERSION)

networking:
  podSubnet: 10.240.0.0/16
  serviceSubnet: 10.110.0.0/16
  disableDefaultCNI: true
  apiServerAddress: "$(IPADDR)"

endef
export KIND_CONFIG


define CALICO_CONFIG
---
apiVersion: operator.tigera.io/v1
kind: Installation
metadata:
  name: default
spec:
  calicoNetwork:
    ipPools:
      - blockSize: 26
        cidr: 10.240.0.0/16
        encapsulation: VXLANCrossSubnet
        natOutgoing: Enabled
        nodeSelector: all()

---
# This section configures the Calico API server.
# For more information, see: https://projectcalico.docs.tigera.io/master/reference/installation/api#operator.tigera.io/v1.APIServer
apiVersion: operator.tigera.io/v1
kind: APIServer
metadata:
  name: default
spec: {}

endef
export CALICO_CONFIG

kind: ## Deploy a Kind based cluster
	@echo "$${KIND_CONFIG}" > $(MINIKUBE_HOME)/.minikube/files/kind-config.yaml
	@if [ ! -f "$(EXE_DIR)/kind" ]; then echo "Kind not Installed - aborted!"; exit 1; fi
	@echo "Kind status:"
	@KIND_EXPERIMENTAL_PROVIDER=$(KIND_DRIVER) $(EXE_DIR)/kind get kubeconfig; rc=$$?; \
	if  [ $$rc != 0 ]; then \
	echo "Kind not running, continuing..."; \
	echo "Using driver: $(KIND_DRIVER) with runtime: $(RUNTIME)"; \
	mkdir -p $(MOUNT_FROM); \
	if [ -n "$(strip $(USE_CACHE))" ]; then \
		export http_proxy=http://$(IPADDR):3128 https_proxy=http://$(IPADDR):3128 no_proxy=$(IPADDR),localhost,127.0.0.1,10.96.0.0/12,192.168.59.0/24,192.168.39.0/24,172.17.0.1/16; \
		export HTTP_PROXY=http://$(IPADDR):3128 HTTPS_PROXY=http://$(IPADDR):3128 NO_PROXY=$(IPADDR),localhost,127.0.0.1,10.96.0.0/12,192.168.59.0/24,192.168.39.0/24,172.17.0.1/16; \
		mkdir -p $(MINIKUBE_HOME)/.minikube/files/etc/ssl/certs; \
		cp $(CACHE_DATA)/docker_mirror_certs/ca.crt $(MINIKUBE_HOME)/.minikube/files/etc/ssl/certs/proxy.crt; \
	fi; \
	if [ -n "$${http_proxy}$${https_proxy}" ]; then \
		export http_proxy=$${http_proxy} https_proxy=$${http_proxy} no_proxy=$${no_proxy}; \
		export HTTP_PROXY=$${http_proxy} HTTPS_PROXY=$${http_proxy} NO_PROXY=$${no_proxy}; \
	fi; \
	echo "Extra ARGS set: $${ENV_VARS} $(ADD_ARGS)"; \
	echo "Local mount: $(MOUNT_FROM):$(MOUNT_TO)"; \
	KIND_EXPERIMENTAL_PROVIDER=$(KIND_DRIVER) $(EXE_DIR)/kind create cluster \
		 --config=$(MINIKUBE_HOME)/.minikube/files/kind-config.yaml \
		 --verbosity 10 \
		 $(FLAGS); \
	else \
		echo -e "\033[1;33mKind already running. If this is unexpected try running: make 'minikube-clean && make all'\033[0m"; \
	fi;
	@echo "$${CALICO_CONFIG}" > $(MINIKUBE_HOME)/.minikube/files/calico-config.yaml
	@helm upgrade calico projectcalico/tigera-operator \
	--version $(CALICO_VERSION) --namespace calico-operator \
	--create-namespace \
	--values $(MINIKUBE_HOME)/.minikube/files/calico-config.yaml \
	--install \
	--wait
	@kubectl wait --namespace calico-operator \
	--for=condition=ready pod \
	--selector=name=tigera-operator \
	--timeout=180s
	@sleep 5
	@kubectl wait --for=condition=ready pod --all --timeout=300s -n calico-system
	@kubectl -n calico-system set env daemonset/calico-node FELIX_IGNORELOOSERPF=true

	@kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/main/deploy/static/provider/kind/deploy.yaml
	kubectl wait --namespace ingress-nginx \
	--for=condition=ready pod \
	--selector=app.kubernetes.io/component=controller \
	--timeout=300s

kind-install: ## ✨ Install Kind, Ingress Controller, Metal LB, and HAProxy (on Linux)
	@make cluster-cache
	@make cluster-registry
	@make kind
	@make cluster-patch-for-metallb
	@make cluster-storage-classes
	@make kind-metallb-config
ifeq ($(strip $(KYVERNO)),yes)
	@make cluster-kyverno
endif
ifeq ($(KIND_DRIVER),podman)
	@make patch-ingress # patch ingress-controller for podman
endif
	@make kind-extdns # deploy the external DNS responder paired with metallb
	@make kind-update-dns # update systemd-resolved config for DNS
ifeq ($(strip $(SKA_TANGO_OPERATOR_ENABLED)),true)
	@make tango-operator
endif
	@make cluster-registry

kind-clean: minikube-reset-docker-env ## 💀 Delete Kind cluster etc. - **caution** there is no going back!
	@make minikube-clean-dns
	@echo "Cleaning up with DRIVER: $(DRIVER)"
ifneq ($(OS_NAME),darwin)
ifeq ($(DRIVER),kvm2)
	@$(MUST_SUDO) $(BIN_DIR)/docker rm -f $(HAPROXY_NAME) 1>/dev/null 2>&1 || true
	@$(MUST_SUDO) $(BIN_DIR)/docker rm -f $(REGISTRY_NAME) 2>/dev/null || true
else
	@$(MUST_SUDO) $(BIN_DIR)/$(DRIVER) rm -f $(HAPROXY_NAME) 1>/dev/null 2>&1 || true
	@$(MUST_SUDO) $(BIN_DIR)/$(DRIVER) rm -f $(REGISTRY_NAME) 2>/dev/null || true
endif
endif
	@KIND_EXPERIMENTAL_PROVIDER=$(KIND_DRIVER) $(EXE_DIR)/kind delete cluster --name skao || true
	@rm -rf $(MINIKUBE_HOME)/.minikube/files/etc/ssl/certs/proxy.crt

kind-all: setup kind-install ## 🔥 do setup and install for Kind
