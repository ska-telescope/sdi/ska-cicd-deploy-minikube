KUEUE_SKA_TANGO_OPERATOR_ENABLED ?= false##decide whether or not to deploy tango operator for Kueue
KUEUE_VERSION ?= v0.10.0## Kueue version to install https://github.com/kubernetes-sigs/kueue/releases
KUEUE_PROM_STACK_VERSION ?= 67.4.0## kube-prometheus-stack version to install https://github.com/prometheus-community/helm-charts/releases

# https://github.com/kubernetes-sigs/kueue/releases/download/v0.10.0/kubectl-kueue-linux-amd64

kueue-setup:  ## install kueue commandline
	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/kueue)
	$(eval RIGHT_KUBECTL := $(shell $(EXE_DIR)/kueue version 2>/dev/null| grep Client | awk '{print $3}' | grep $(KUEUE_VERSION)))
	@if [ ! -f "$(EXE_DIR)/kueue" ] || [ -z "$(RIGHT_KUBECTL)" ]; then \
		echo "Installing kueue version $(KUEUE_VERSION)"; \
		curl -Lo $(TMP_FILE) https://github.com/kubernetes-sigs/kueue/releases/download/$(KUEUE_VERSION)/kubectl-kueue-$(OS_NAME)-$(OS_BIN) \
		&& $(SUDO_FOR_EXE_DIR) mv -f $(TMP_FILE) $(EXE_DIR)/kueue  \
		&& $(SUDO_FOR_EXE_DIR) chmod +x $(EXE_DIR)/kueue; \
	else \
		echo "kueue $(KUEUE_VERSION) already installed"; \
	fi
	@rm -rf $(TEMPDIR)

kueue-clean:  ## Clean up after Flux demonstration
	make clean PROFILE=minikube || true

kueue-all:  ## build minikube , deploy fluxcd and run local example
	make all PROFILE=minikube KYVERNO=no SKA_TANGO_OPERATOR_ENABLED=$(KUEUE_SKA_TANGO_OPERATOR_ENABLED)
	make kueue-setup
	make kueue-install-prom
	make kueue-install
	make kueue-examples

kueue-install-prom:  ## Install Prom Stack in Minikube
	helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
	helm repo update prometheus-community
	helm --kube-context $(PROFILE) upgrade prometheus prometheus-community/kube-prometheus-stack \
		--version $(KUEUE_PROM_STACK_VERSION) \
		--install \
		--create-namespace \
		--namespace prometheus \
		 --set grafana.service.type=LoadBalancer
	@sleep 3
	kubectl --context $(PROFILE) -n prometheus rollout status deployment prometheus-kube-prometheus-operator --timeout=5m
	@sleep 10
	kubectl --context $(PROFILE) -n prometheus wait -l statefulset.kubernetes.io/pod-name=prometheus-prometheus-kube-prometheus-prometheus-0 --for=condition=ready pod --timeout=5m
	@PROM_USER=`kubectl --context $(PROFILE) -n prometheus get secret prometheus-grafana -o jsonpath="{.data.admin-user}" | base64 --decode`; \
	echo "Grafana user: $${PROM_USER}"
	@PROM_PASSWORD=`kubectl --context $(PROFILE) -n prometheus get secret prometheus-grafana -o jsonpath="{.data.admin-password}" | base64 --decode`; \
	echo "Grafana password: $${PROM_PASSWORD}"

kueue-install:  ## Install Kueue in Minikube
	# kubectl --context $(PROFILE) apply --server-side -f https://github.com/kubernetes-sigs/kueue/releases/download/$(KUEUE_VERSION)/manifests.yaml
	$(eval TEMPDIR := $(shell mktemp -d))
	cd $(TEMPDIR) && \
	curl --silent --location https://github.com/kubernetes-sigs/kueue/releases/download/$(KUEUE_VERSION)/manifests.yaml | \
	yq -s '"file_" + $$index' -
	cd $(TEMPDIR) && CM_FILE=`grep -l 'kind: ConfigMap' *`; \
	echo $$CM_FILE; \
	echo "$${TEMPLATE_KUEUE_CONFIG}" > $$CM_FILE; \
	cat $$CM_FILE; \
	cat *.yml | kubectl --context $(PROFILE) apply --server-side -f -
	find $(TEMPDIR)
	@rm -rf $(TEMPDIR)
	sleep 3
	kubectl --context $(PROFILE) wait deploy/kueue-controller-manager -n kueue-system \
		--for=condition=available --timeout=10m

	kubectl --context $(PROFILE) apply --server-side -f https://github.com/kubernetes-sigs/kueue/releases/download/$(KUEUE_VERSION)/prometheus.yaml
	sleep 3
	kubectl --context $(PROFILE) apply -k "https://github.com/kubernetes-sigs/kueue.git/config/prometheus?ref=$(KUEUE_VERSION)&timeout=300s"
	kubectl --context $(PROFILE) apply --server-side -f https://github.com/kubernetes-sigs/kueue/releases/download/$(KUEUE_VERSION)/visibility-apf.yaml
	sleep 15

kueue-viz:  ## install kueue-viz backend - it's experimental!!!
	# https://github.com/kubernetes-sigs/kueue/tree/main/cmd/experimental/kueue-viz
	git clone -b $(KUEUE_VERSION) --single-branch git@github.com:kubernetes-sigs/kueue.git
	kubectl --context $(PROFILE) create clusterrole kueue-backend-read-access \
					--verb=get,list,watch \
          --resource=workloads,clusterqueues,localqueues,resourceflavors,pods,workloadpriorityclass,events,nodes

	$(eval TEMPDIR := $(shell mktemp -d))
	@echo "$${TEMPLATE_KUEUE_VIZ_AUTH}" > $(TEMPDIR)/kueue-vz-auth.yaml
	kubectl --context $(PROFILE) applt -f $(TEMPDIR)/kueue-vz-auth.yaml
	@rm -rf $(TEMPDIR)

	go install github.com/githubnemo/CompileDaemon@latest
	cd kueue/cmd/experimental/kueue-viz/backend && make debug

kueue-viz-frontend: ## install kueue-viz backend - it's experimental!!!
	cd kueue/cmd/experimental/kueue-viz/frontend && npm install react-scripts
	cd kueue/cmd/experimental/kueue-viz/frontend && make debug

kueue-examples: ## Deploy kueue examples
# https://kueue.sigs.k8s.io/docs/tasks/manage/monitor_pending_workloads/pending_workloads_on_demand/#configure-api-priority-and-fairness
	kubectl --context $(PROFILE) apply -f https://kueue.sigs.k8s.io/examples/admin/single-clusterqueue-setup.yaml
	for i in {1..6}; do kubectl create -f https://kueue.sigs.k8s.io/examples/jobs/sample-job.yaml; done

	kubectl --context $(PROFILE) get --raw "/apis/visibility.kueue.x-k8s.io/v1beta1/clusterqueues/cluster-queue/pendingworkloads"

define TEMPLATE_KUEUE_VIZ_AUTH
---
apiVersion: rbac.authorization.k8s.io/v1
kind: ClusterRoleBinding
metadata:
  name: kueue-backend-read-access-binding
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: ClusterRole
  name: kueue-backend-read-access
subjects:
- kind: ServiceAccount
  name: default
  namespace: kueue-viz

endef
export TEMPLATE_KUEUE_VIZ_AUTH




define TEMPLATE_KUEUE_CONFIG
---
apiVersion: v1
data:
  controller_manager_config.yaml: |
    apiVersion: config.kueue.x-k8s.io/v1beta1
    kind: Configuration
    health:
      healthProbeBindAddress: :8081
    metrics:
      bindAddress: :8080
    # enableClusterQueueResources: true
    webhook:
      port: 9443
    leaderElection:
      leaderElect: true
      resourceName: c1f6bfd2.kueue.x-k8s.io
    controller:
      groupKindConcurrency:
        Job.batch: 5
        Pod: 5
        Workload.kueue.x-k8s.io: 5
        LocalQueue.kueue.x-k8s.io: 1
        ClusterQueue.kueue.x-k8s.io: 1
        ResourceFlavor.kueue.x-k8s.io: 1
    clientConnection:
      qps: 50
      burst: 100
    #pprofBindAddress: :8083
    #waitForPodsReady:
    #  enable: false
    #  timeout: 5m
    #  blockAdmission: false
    #  requeuingStrategy:
    #    timestamp: Eviction
    #    backoffLimitCount: null # null indicates infinite requeuing
    #    backoffBaseSeconds: 60
    #    backoffMaxSeconds: 3600
    #manageJobsWithoutQueueName: true
    #internalCertManagement:
    #  enable: false
    #  webhookServiceName: ""
    #  webhookSecretName: ""
    managedJobsNamespaceSelector:
      matchExpressions:
        - key: kubernetes.io/metadata.name
          operator: NotIn
          values: [ kueue-system, prometheus, cilium-secrets, extdns, ingress-nginx, kube-node-lease, kube-public, kube-system, kyverno, metallb-system, ska-tango-operator, default ]
    integrations:
      frameworks:
      - "batch/job"
      - "kubeflow.org/mpijob"
      - "ray.io/rayjob"
      - "ray.io/raycluster"
      - "jobset.x-k8s.io/jobset"
      - "kubeflow.org/mxjob"
      - "kubeflow.org/paddlejob"
      - "kubeflow.org/pytorchjob"
      - "kubeflow.org/tfjob"
      - "kubeflow.org/xgboostjob"
      - "pod"
    #  externalFrameworks:
    #  - "Foo.v1.example.com"
      # podOptions:
      #   namespaceSelector:
      #     matchExpressions:
      #       - key: kubernetes.io/metadata.name
      #         operator: NotIn
      #         values: [ kueue-system, prometheus, cilium-secrets, extdns, ingress-nginx, kube-node-lease, kube-public, kube-system, kyverno, metallb-system, ska-tango-operator, default ]
    fairSharing:
      enable: true
      preemptionStrategies: [LessThanOrEqualToFinalShare, LessThanInitialShare]
    resources:
      excludeResourcePrefixes: []
kind: ConfigMap
metadata:
  labels:
    app.kubernetes.io/component: controller
    app.kubernetes.io/name: kueue
    control-plane: controller-manager
  name: kueue-manager-config
  namespace: kueue-system

endef
export TEMPLATE_KUEUE_CONFIG

