#!/usr/bin/env bash

# load images from a given list of charts in the CAR, or a chart in a local directory.
# pass list of charts as arguments to this script
K8S_CHARTS=${@:-ska-tango-base}

CAR="https://artefact.skao.int/repository/helm-internal"
REPO_NAME="ska"
EXE_DIR=${EXE_DIR:-/usr/local/bin}

# do we have the correct yq version
if [[ -z `${EXE_DIR}/yq --version 2>/dev/null | grep mikefarah 2>/dev/null` ]]; then
    echo "You probably have the wrong version of yq installed - pip3? uninstall yq; make setup; or see https://github.com/mikefarah/yq"
    exit 1
fi

# add the repository if it doesn't exist, and update
(${EXE_DIR}/helm repo list | grep "${REPO_NAME}" >/dev/null) || \
${EXE_DIR}/helm repo add ${REPO_NAME} ${CAR}
${EXE_DIR}/helm repo update ${REPO_NAME} >/dev/null 2>&1

# hunt for images in the templated charts using yq
echo "Searching in charts: ${K8S_CHARTS}"
temp_file=$(mktemp images.XXXXXX)
for chart in ${K8S_CHARTS}; do
    echo "Checking chart: ${chart}"
    if [[ -d "${chart}" ]]; then
        tmp_dir=$(mktemp -d -t chart.XXXXXXXXXX)
        ${EXE_DIR}/helm template --values resources/gitlab_values.yaml ${K8S_CHART_PARAMS} --output-dir ${tmp_dir} test ${chart}
    else
        tmp_dir=$(mktemp -d -t chart-${chart}.XXXXXXXXXX)
        ${EXE_DIR}/helm template --values resources/gitlab_values.yaml ${K8S_CHART_PARAMS} --output-dir ${tmp_dir} test ${REPO_NAME}/${chart}
    fi
    for dir in `find ${tmp_dir} -name templates -type d`; do
        for template in ${dir}/*.yaml; do
            echo "looking for images in: ${template}"
            cat ${template} | ${EXE_DIR}/yq e ".spec.template.spec.initContainers[].image" - >> ${temp_file} 2>/dev/null
            cat ${template} | ${EXE_DIR}/yq e ".spec.template.spec.containers[].image" - >> ${temp_file} 2>/dev/null
        done
    done
    rm -rf ${tmp_dir}
done
echo "Combined list of charts to load:"
cat ${temp_file} | sort | uniq | grep -v '\-\-\-'
echo ""

echo "Finished searching charts, now loading images"
for image in `cat ${temp_file} | sort | uniq | grep -v '\-\-\-'`; do
    echo "loading image: ${image}"
    ${EXE_DIR}/minikube image load --overwrite=true --remote=false --pull=false "${image}"
done
rm -f ${temp_file}
echo ""

echo "List of loaded images:"
${EXE_DIR}/minikube image ls

echo ""
echo "To force reload an image use: minikube image load <image name>:<tag>"
