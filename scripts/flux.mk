FLUX_SKA_TANGO_OPERATOR_ENABLED=false
FLUX_VERSION ?= 2.3.0## Flux CD tool version to install https://github.com/fluxcd/flux2/releases

flux-clean:  ## Clean up after Flux demonstration
	make clean PROFILE=north || true
	make clean PROFILE=minikube || true

flux-all:  ## build minikube and north, deploy fluxcd and run local and remote example
	make all PROFILE=minikube KYVERNO=no SKA_TANGO_OPERATOR_ENABLED=$(FLUX_SKA_TANGO_OPERATOR_ENABLED)
	make flux-setup
	make flux-install
	make all PROFILE=north KYVERNO=no SKA_TANGO_OPERATOR_ENABLED=$(FLUX_SKA_TANGO_OPERATOR_ENABLED)
	for i in \
		registry.gitlab.com/ska-telescope/ska-tango-operator/ska-tango-operator:0.9.11 \
		artefact.skao.int/ska-tango-images-tango-db:11.0.2 \
		artefact.skao.int/ska-tango-images-tango-cpp:9.4.2 \
		artefact.skao.int/ska-tango-examples:0.4.31 \
		artefact.skao.int/ska-tango-images-tango-dsconfig:1.5.13 \
		artefact.skao.int/ska-tango-images-tango-itango:9.5.0 \
		docker.io/alpine:3.12 ; do \
		echo "Loading: $${i}"; \
		$(DRIVER) pull $${i}; \
		minikube -p north image load $${i} --daemon; \
	done
	make flux-examples-remote


flux-all-local:  ## build minikube , deploy fluxcd and run local example
	make all PROFILE=minikube KYVERNO=no SKA_TANGO_OPERATOR_ENABLED=$(FLUX_SKA_TANGO_OPERATOR_ENABLED)
	make flux-setup
	make flux-install
	for i in \
		registry.gitlab.com/ska-telescope/ska-tango-operator/ska-tango-operator:0.9.11 \
		artefact.skao.int/ska-tango-images-tango-db:11.0.2 \
		artefact.skao.int/ska-tango-images-tango-cpp:9.4.2 \
		artefact.skao.int/ska-tango-examples:0.4.31 \
		artefact.skao.int/ska-tango-images-tango-dsconfig:1.5.13 \
		artefact.skao.int/ska-tango-images-tango-itango:9.5.0 \
		docker.io/alpine:3.12 ; do \
		echo "Loading: $${i}"; \
		$(DRIVER) pull $${i}; \
		minikube -p minikube image load $${i} --daemon; \
	done
	make flux-examples-local

flux-setup:  ## Install Flux CD commandline
	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/flux.tar.gz)
	$(eval RIGHT_FLUX := $(shell $(EXE_DIR)/flux -v 2>&1 | grep "flux version $(FLUX_VERSION)"))
	@if [ ! -f "$(EXE_DIR)/flux" ] || [ -z "$(RIGHT_FLUX)" ]; then \
		echo "Installing flux version $(FLUX_VERSION)"; \
		curl -Lo $(TMP_FILE) https://github.com/fluxcd/flux2/releases/download/v$(FLUX_VERSION)/flux_$(FLUX_VERSION)_$(OS_NAME)_$(OS_BIN).tar.gz \
		&& tar -xzOf $(TMP_FILE) flux > /tmp/flux && $(SUDO_FOR_EXE_DIR) mv -f /tmp/flux $(EXE_DIR)/flux  \
		&& $(SUDO_FOR_EXE_DIR) chmod +x $(EXE_DIR)/flux; \
	else \
		echo "flux $(FLUX_VERSION) already installed"; \
	fi
	@rm -rf $(TEMPDIR)

flux-install:  ## Install Flux CD in Minikube
	helm install --kube-context minikube -n flux-system  \
	  --create-namespace flux oci://ghcr.io/fluxcd-community/charts/flux2
	sleep 15
	kubectl -n flux-system apply -f resources/flux-capacitor.yaml
	@echo " to access flux-capacitor: kubectl --context minikube -n flux-system port-forward svc/capacitor 9000:9000"

flux-examples-local: ## Deploy local full example of Tango in minikkube
	kubectl config use-context minikube
	for i in flux-system ska-tango-operator examples; do \
	  kubectl --context minikube create ns $${i} || true ; \
	done
	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/ska.yaml)
	cp -f resources/flux-ska-tango-examples.yaml $(TMP_FILE)
	KC=`make cluster-kubeconfig PROFILE=minikube | awk '{ print "    " $$0 }'`; \
	echo "$${KC}" >> $(TMP_FILE)
	kubectl config use-context minikube
	kubectl --context minikube apply -f $(TMP_FILE)
	@rm -rf $(TEMPDIR)

flux-test-local:  ## Deploy simple local test
	kubectl create ns flux-system || true
	kubectl apply -f resources/flux-helmrelease-local.yaml

flux-test-remote:  ## Deploy simple remote test in North
	kubectl config use-context north
	kubectl create ns flux-system || true
	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/fluxhr.yaml)
	cp -f resources/flux-helmrelease-remote.yaml $(TMP_FILE)
	KC=`make cluster-kubeconfig PROFILE=north | awk '{ print "    " $$0 }'`; \
	echo "$${KC}" >> $(TMP_FILE)
	kubectl config use-context minikube
	kubectl --context minikube apply -f $(TMP_FILE)
	@rm -rf $(TEMPDIR)

flux-examples-remote: ## Deploy remote full example of Tango in North
	kubectl config use-context north
	for i in flux-system ska-tango-operator examples; do \
	  kubectl --context north create ns $${i} || true ; \
	done
	$(eval TEMPDIR := $(shell mktemp -d))
	$(eval TMP_FILE:= $(TEMPDIR)/ska.yaml)
	cp -f resources/flux-ska-tango-examples.yaml $(TMP_FILE)
	KC=`make cluster-kubeconfig PROFILE=north | awk '{ print "    " $$0 }'`; \
	echo "$${KC}" >> $(TMP_FILE)
	kubectl config use-context minikube
	kubectl --context minikube apply -f $(TMP_FILE)
	@rm -rf $(TEMPDIR)
