#!/bin/sh

NAMESPACE=default
NO_PROXY=`minikube ip`,localhost
CLASS=${CLASS:-nginx}

wait () {
    printf "*------------------------------ pause --------------------------------*\n\n"
    read X
}

echo "Check the Kubernetes cluster:"
printf "Connecting using KUBECONFIG=${KUBECONFIG}\n\nVersion Details:\n"
kubectl version
printf "\nList nodes:\n"
kubectl get nodes -o wide

printf "\nCheck the Ingress connection details:\n"
# get the external IP for the main loadbalancer - hopefully there is only one
# this then points to the Ingress controller
# If there isn't one then look for the NodePort implementation of NGINX
if [ "${CLASS}" = "nginx" ]; then
  host=`minikube ip`
  TMP_EXTERNAL_IP="${host}:80"
else
  TMP_EXTERNAL_IP=`kubectl get svc -A -o jsonpath="{.items[*].spec.externalIPs[0]}"`
  if [ -z "${TMP_EXTERNAL_IP}" ]; then
      SVC_TYPE=`kubectl get svc traefik0 -n kube-system -o jsonpath="{.spec.type}"`
      if [ "${SVC_TYPE}" = "NodePort" ]; then
          host=`kubectl cluster-info | grep 'control plane' | cut -d/ -f3 | cut -d: -f1`
          port=`kubectl get svc traefik0 -n kube-system -o jsonpath="{.spec.ports[?(@.name=='http')].nodePort}"`
          TMP_EXTERNAL_IP="${host}:${port}"
      else
          SVC_TYPE=`kubectl get svc ingress-nginx -n ingress-nginx -o jsonpath="{.spec.type}"`
          if [ "${SVC_TYPE}" = "NodePort" ]; then
              host=`kubectl cluster-info | grep 'control plane' | cut -d/ -f3 | cut -d: -f1`
              port=`kubectl get svc ingress-nginx -n ingress-nginx -o jsonpath="{.spec.ports[?(@.name=='http')].nodePort}"`
              TMP_EXTERNAL_IP="${host}:${port}"
          fi
      fi
  fi
fi
EXTERNAL_IP=${EXTERNAL_IP:-${TMP_EXTERNAL_IP}}
echo "Ingress Controller LoadBalancer externalIP is: ${EXTERNAL_IP}"

printf "\n\nShow StorageClasses:\n"
kubectl get sc -o wide
printf "\nNext: show StorageClass details.\n"
# wait

printf "\nCheck Ingress Controller is ready:\n"
kubectl -n ingress-nginx wait deploy/ingress-nginx-controller --for condition=available --timeout=180s

printf "\nDeploy the Integration test:"
# create/test/destroy
cat <<EOF | sed "s/XCLASSX/${CLASS}/g" | kubectl -n ${NAMESPACE} apply -f -
---
apiVersion: v1
kind: PersistentVolume
metadata:
  name: pvtest
  labels:
    type: local
spec:
  storageClassName: standard
  persistentVolumeReclaimPolicy: Delete
  capacity:
    storage: 1Gi
  accessModes:
    - ReadWriteOnce
  hostPath:
    path: "/mnt/pvtest"

---
apiVersion: v1
kind: PersistentVolumeClaim
metadata:
  name: pvc-test
spec:
  storageClassName: standard
  accessModes:
    - ReadWriteOnce
  resources:
    requests:
      storage: 1Gi
  volumeName: pvtest

---
apiVersion: v1
kind: ConfigMap
metadata:
  name: test
data:
  hello.conf: |
    server {
        listen 80;

        root /usr/share/nginx/html;
        try_files /index.html =404;

        expires -1;

        sub_filter_once off;
        sub_filter 'server_hostname' '$hostname';
        sub_filter 'server_address' '$server_addr:$server_port';
        sub_filter 'server_url' '$request_uri';
        sub_filter 'server_date' '$time_local';
        sub_filter 'request_id' '$request_id';
    }

---
apiVersion: v1
kind: Service
metadata:
  name: nginx1
  labels:
    app: nginx1
    app.kubernetes.io/name: test
spec:
  selector:
    app: nginx1
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment1
  labels:
    app: nginx1
    app.kubernetes.io/name: test
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx1
  template:
    metadata:
      labels:
        app: nginx1
        app.kubernetes.io/name: test
    spec:
      containers:
      - name: nginx
        image: nginx
        imagePullPolicy: Always
        ports:
          - containerPort: 80
            protocol: TCP
        volumeMounts:
          - mountPath: /usr/share/nginx/html
            name: www-data
            #readOnly: true
          - name: test-config
            mountPath: /etc/nginx/conf.d
      volumes:
      - name: www-data
        persistentVolumeClaim:
          claimName: pvc-test
      - name: test-config
        configMap:
          name: test

---
apiVersion: v1
kind: Service
metadata:
  name: nginx2
  labels:
    app: nginx2
    app.kubernetes.io/name: test
spec:
  selector:
    app: nginx2
  ports:
    - protocol: TCP
      port: 80
      targetPort: 80

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: nginx-deployment2
  labels:
    app: nginx2
    app.kubernetes.io/name: test
spec:
  replicas: 3
  selector:
    matchLabels:
      app: nginx2
  template:
    metadata:
      labels:
        app: nginx2
        app.kubernetes.io/name: test
    spec:
      containers:
      - name: nginx
        image: nginx
        imagePullPolicy: Always
        ports:
          - containerPort: 80
            protocol: TCP
        volumeMounts:
          - mountPath: /usr/share/nginx/html
            name: www-data
            readOnly: true
          - name: test-config
            mountPath: /etc/nginx/conf.d
      volumes:
      - name: www-data
        persistentVolumeClaim:
          claimName: pvc-test
      - name: test-config
        configMap:
          name: test

---
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: test
  labels:
    app.kubernetes.io/name: test
  annotations:
    kubernetes.io/ingress.class: "XCLASSX"
spec:
  rules:
  - host: nginx1
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: nginx1
            port:
              number: 80
  - host: nginx2
    http:
      paths:
      - path: /
        pathType: Prefix
        backend:
          service:
            name: nginx2
            port:
              number: 80
EOF
kubectl -n ${NAMESPACE} get deployment,pod,svc,ingress.networking.k8s.io,pvc -l app.kubernetes.io/name=test -o wide
printf "\nNext: Check deployment.\n"
# wait

echo "Waiting for resources to deploy..."
kubectl -n ${NAMESPACE} wait --for=condition=available deployment.v1.apps/nginx-deployment1 --timeout=180s
kubectl -n ${NAMESPACE} wait --for=condition=available deployment.v1.apps/nginx-deployment2 --timeout=180s

# Expose a metallb LoadBalancer
kubectl expose deploy nginx-deployment1 --port 80 --type LoadBalancer

kubectl -n ${NAMESPACE} get deployment,pod,svc,ingress.networking.k8s.io,pvc -l app.kubernetes.io/name=test -o wide
printf "\nNext: perform write/read test.\n"
# wait

DTE=`date`
echo "Perform write and then read test to/from shared storage -expected date stamp: ${DTE}"
printf "\necho \"echo '${DTE}' > /usr/share/nginx/html/index.html\" | kubectl -n \${NAMESPACE} exec -i \$(kubectl get pods -l app=nginx1 -o name | head -1) -- bash\n\n"

echo "echo '${DTE}' > /usr/share/nginx/html/index.html" | kubectl -n ${NAMESPACE} exec -i $(kubectl get pods -l app=nginx1 -o name | head -1) -- bash

echo "Waiting for LoadBalancer ..."
ip=""
while [ -z $ip ]; do
  echo "Waiting for external IP"
  ip=$(kubectl get svc nginx-deployment1 --namespace ${NAMESPACE} --template="{{range .status.loadBalancer.ingress}}{{.ip}}{{end}}")
  [ -z "$ip" ] && sleep 3
done
echo 'Found external IP: '$ip
sleep 5

echo ''
echo "Test Ingress -> Deployment: nginx1"
echo '----------------------------------------nginx1----------------------------------------'
echo "no_proxy=${NO_PROXY} curl -s -H \"Host: nginx1\" http://${EXTERNAL_IP}/"
res=`no_proxy=${NO_PROXY} curl -s -H "Host: nginx1" http://${EXTERNAL_IP}/`
nginx1_rc=$?
echo "curl Ingress for nginx1 rc: ${nginx1_rc}"
total_rc=$?
if [[ "${DTE}" == "${res}" ]]; then
  echo "Received: ${res} == ${DTE} - OK"
else
  echo "Received: ${res} not = ${DTE} - Something went wrong!"
  total_rc=1
fi
echo ''
echo "Test Ingress -> Deployment: nginx2"
echo '----------------------------------------nginx2----------------------------------------'
echo "no_proxy=${NO_PROXY} curl -s -H \"Host: nginx2\" http://${EXTERNAL_IP}/"
res=`no_proxy=${NO_PROXY} curl -s -H "Host: nginx2" http://${EXTERNAL_IP}/`
nginx2_rc=$?
echo "curl Ingress for nginx2 rc: ${nginx2_rc}"
if [[ "${DTE}" == "${res}" ]]; then
  echo "Received: ${res} == ${DTE} - OK"
else
  echo "Received: ${res} not = ${DTE} - Something went wrong!"
  total_rc=1
fi
echo ''

METALLB_IP=`kubectl get svc/nginx-deployment1 --template="{{range .status.loadBalancer.ingress}}{{.ip}}{{end}}"`
echo ''
echo "Test metallb LoadBalancer"
echo '----------------------------------nginx-deployment1-----------------------------------'
echo "no_proxy=${NO_PROXY} curl -s http://${METALLB_IP}/"
res=`no_proxy=${NO_PROXY} curl -s http://${METALLB_IP}/`
lb_rc=$?
echo "curl LoadBalancer rc: ${lb_rc}"
if [[ "${DTE}" == "${res}" ]]; then
  echo "Received: ${res} == ${DTE} - OK"
else
  echo "Received: ${res} not = ${DTE} - Something went wrong!"
  total_rc=1
fi
echo ''

# wait

echo "Cleanup resources"
kubectl -n ${NAMESPACE} delete --ignore-not-found \
  ingress.networking.k8s.io/test \
  svc/nginx-deployment1 \
  service/nginx1 \
  deployment.apps/nginx-deployment1 \
  service/nginx2 \
  deployment.apps/nginx-deployment2 \
  persistentvolumeclaim/pvc-test \
  persistentvolume/pvtest \
  configmap/test

# factor in the curl return codes from each test above
if [[ $nginx1_rc -ne 0 ]] || [[ $nginx2_rc -ne 0 ]] || [[ $lb_rc -ne 0 ]]; then
  echo "Error detected on curl calls!"
  total_rc=1
fi
echo "Overall exit code is: ${total_rc}"
exit ${total_rc}
