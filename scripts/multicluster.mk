# Multicluster mesh demo with Cilium
#
MULTICLUSTER_VM_NAME ?= cew
MULTICLUSTER_VM_IMAGE ?= gcr.io/k8s-minikube/kicbase:v0.0.45

multicluster-all: multicluster multicluster-configure-mesh

multicluster-full-demo: ## run complete demo of multicluster including off cluster
	make multicluster-clean
	make multicluster-all
	gnome-terminal --window -x bash -c "k9s --context north -A -c pods" || true
	gnome-terminal --window -x bash -c "k9s --context south -A -c pods" || true
	make multicluster-demo
	make multicluster-vm-all

multicluster-vars ?= MEM=4096 CPUS=4 CILIUM=yes KYVERNO=no SKA_TANGO_OPERATOR_ENABLED=false TOOLS=no
multicluster-clean:  ## Clean up after Flux demonstration
	make clean PROFILE=north || true
	make clean PROFILE=south || true
	make multicluster-clean-vm

multicluster:  ## build north and south
	$(multicluster-vars) make all PROFILE=north CLUSTER_DOMAIN=north.local CILIUM_CLUSTER_ID=255 ADD_ARGS=--extra-config=kubeadm.pod-network-cidr=10.0.0.0/16
	kubectl --context north -n kube-system get cm coredns -o yaml | \
	grep -v 'rewrite name' | \
	sed '/ready$$/a\        rewrite name substring south.local north.local' | \
	kubectl --context north -n kube-system apply -f -
	kubectl --context north rollout restart -n kube-system deployment/coredns
	$(multicluster-vars) make all PROFILE=south CLUSTER_DOMAIN=south.local CILIUM_CLUSTER_ID=1 ADD_ARGS=--extra-config=kubeadm.pod-network-cidr=10.1.0.0/16

multicluster-test:  ## test north and south
	cilium connectivity test --context north
	cilium connectivity test --context south

multicluster-configure-mesh: ## Configure cluster mesh
	# replicate the CA from the core to the rest of the mesh
	# kubectl --context=north get secret -n kube-system cilium-ca -o yaml | \
  	# kubectl --context south create -f -

	for i in north south; do \
		cilium --context $${i} status; \
		cilium --context $${i} clustermesh enable \
			--enable-kvstoremesh=true \
			--enable-external-workloads \
			--service-type LoadBalancer; \
		cilium --context $${i} clustermesh status --wait; \
	done
	cilium clustermesh connect --context north \
		--destination-context south
	cilium --context north clustermesh status --wait
	# cilium connectivity test --context north --multi-cluster south

multicluster-clean-vm: ## Remove a kicbase container that emulates a VM
	$(RUNTIME) rm -f $(MULTICLUSTER_VM_NAME) || true

multicluster-vm-ssh:
	ssh root@$$($(RUNTIME) inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' $(MULTICLUSTER_VM_NAME))

multicluster-create-vm: ## Create a kicbase container to emulate a VM
	make multicluster-clean-vm
	sleep 1
	$(RUNTIME) run --rm \
	-d -t \
	--privileged \
	--security-opt seccomp=unconfined \
	--tmpfs /tmp \
	--tmpfs /run \
	--volume /lib/modules:/lib/modules:ro \
	--hostname $(MULTICLUSTER_VM_NAME) \
	--name $(MULTICLUSTER_VM_NAME) \
	--memory=8192mb \
	-e container=$(RUNTIME) \
	--expose 22 \
	$(MULTICLUSTER_VM_IMAGE)
	sleep 3
	$(RUNTIME) exec -ti $(MULTICLUSTER_VM_NAME) bash -c "mkdir -p /root/.ssh; touch /root/.ssh/authorized_keys; chmod 600 /root/.ssh/authorized_keys"
	ssh-add -L | $(RUNTIME) exec -i $(MULTICLUSTER_VM_NAME) bash -c "cat - >>/root/.ssh/authorized_keys"

multicluster-configure-vm: ## Configure external workload VM
	cilium --context south clustermesh \
		vm create $(MULTICLUSTER_VM_NAME) -n default --ipv4-alloc-cidr 10.192.1.0/30
	sleep 3
	cilium --context south clustermesh vm status
	cilium --context south clustermesh vm install /tmp/install-external-workload.sh
	$(RUNTIME) cp /tmp/install-external-workload.sh $(MULTICLUSTER_VM_NAME):/root/install-external-workload.sh
	$(RUNTIME) exec -ti $(MULTICLUSTER_VM_NAME) bash -c "/root/install-external-workload.sh"
	$(RUNTIME) exec -ti $(MULTICLUSTER_VM_NAME) bash -c "sed -e 's/^#*/#/' /etc/resolv.conf > /etc/resolv.conf.tmp"
	$(RUNTIME) exec -ti $(MULTICLUSTER_VM_NAME) bash -c "cat /etc/resolv.conf.tmp > /etc/resolv.conf"
	extdns=$$(kubectl --context south -n extdns get svc extdns-coredns -o jsonpath={.status.loadBalancer.ingress[0].ip}); \
	echo -e "nameserver 10.96.0.10\nnameserver $${extdns}\nsearch default.svc.cluster.local svc.cluster.local cluster.local\n" | \
		$(RUNTIME) exec -i $(MULTICLUSTER_VM_NAME) bash -c "cat - >>/etc/resolv.conf"

	$(RUNTIME) exec -ti $(MULTICLUSTER_VM_NAME) bash -c "cilium-dbg status"
	$(RUNTIME) exec -ti $(MULTICLUSTER_VM_NAME) bash -c "dig clustermesh-apiserver.kube-system.svc.south.local"

multicluster-load-image-to-vm: ## preload local cached cilium image for speed
	make cluster-registry
	$(RUNTIME) pull quay.io/cilium/cilium:$(CILIUM_VERSION)
	$(RUNTIME) tag quay.io/cilium/cilium:$(CILIUM_VERSION) localhost:5000/cilium:$(CILIUM_VERSION)
	$(RUNTIME) push localhost:5000/cilium:$(CILIUM_VERSION)
	echo -e '{"insecure-registries": ["$(IP):5000"],"registry-mirrors": ["http://$(IP):5000"]}\n' | \
	$(RUNTIME) exec -i $(MULTICLUSTER_VM_NAME) bash -c "cat - >/etc/docker/daemon.json"
	$(RUNTIME) exec -ti $(MULTICLUSTER_VM_NAME) bash -c "systemctl restart docker.service"
	$(RUNTIME) exec -ti $(MULTICLUSTER_VM_NAME) bash -c "$(RUNTIME) pull $(IP):5000/cilium:$(CILIUM_VERSION)"
	$(RUNTIME) exec -ti $(MULTICLUSTER_VM_NAME) bash -c "$(RUNTIME) tag $(IP):5000/cilium:$(CILIUM_VERSION) quay.io/cilium/cilium:$(CILIUM_VERSION)"


multicluster-demo-south-external: ## Run demo with external workload pointing to South
# https://docs.cilium.io/en/stable/network/clustermesh/services/#gs-clustermesh-services

	$(eval TEMPDIR := $(shell mktemp -d))
	@echo "$${MULTICLUSTER_TEST2}" > $(TEMPDIR)/cluster2.yaml
	kubectl --context south apply -f $(TEMPDIR)/cluster2.yaml
	kubectl --context south wait --for=condition=ready pod --all --timeout=180s -n default
	$(RUNTIME) exec -ti $(MULTICLUSTER_VM_NAME) bash -c "cilium-dbg status"
	$(RUNTIME) exec -ti $(MULTICLUSTER_VM_NAME) bash -c "dig rebel-base.default.svc.south.local"
	$(RUNTIME) exec -ti $(MULTICLUSTER_VM_NAME) bash -c "curl rebel-base.default.svc.south.local"

# https://docs.cilium.io/en/stable/network/external-workloads/
multicluster-vm-all: multicluster-create-vm multicluster-load-image-to-vm multicluster-configure-vm multicluster-demo-south

multicluster-demo: ## Run demo with shadow service in North pointing to South
# https://docs.cilium.io/en/stable/network/clustermesh/services/#gs-clustermesh-services

	$(eval TEMPDIR := $(shell mktemp -d))
	@echo "$${MULTICLUSTER_TEST1}" > $(TEMPDIR)/cluster1.yaml
	@echo "$${MULTICLUSTER_TEST2}" > $(TEMPDIR)/cluster2.yaml
	@echo "$${MULTICLUSTER_POLICY1}" > $(TEMPDIR)/policy1.yaml
	@echo "$${MULTICLUSTER_POLICY2}" > $(TEMPDIR)/policy2.yaml

	kubectl --context north apply -f $(TEMPDIR)/cluster1.yaml
	kubectl --context south apply -f $(TEMPDIR)/cluster2.yaml
	kubectl --context north wait --for=condition=ready pod --all --timeout=180s -n default
	kubectl --context south wait --for=condition=ready pod --all --timeout=180s -n default
	kubectl --context north apply -f $(TEMPDIR)/policy1.yaml
	kubectl --context south apply -f $(TEMPDIR)/policy1.yaml
	kubectl --context south apply -f $(TEMPDIR)/policy2.yaml
	sleep 5
	for c in north south; do \
		for i in $(shell seq 1 1 3); do \
			echo "Pinging from cluster: $${c} -> $$(kubectl --context $${c} exec -ti deployment/x-wing -- curl --connect-timeout 3 rebel-base)"; \
		done; \
	done
	echo "Ping from north to south using FQDN (of north): $$(kubectl --context north exec -ti deployment/x-wing -- curl --connect-timeout 3 rebel-base.default.svc.north.local)"
	echo "Ping from north to south using FQDN (of south): $$(kubectl --context north exec -ti deployment/x-wing -- curl --connect-timeout 3 rebel-base.default.svc.south.local)"

	echo "Ping from north to south using FQDN (of north - will fail due to policy): $$(kubectl --context north exec -ti deployment/tie-fighter -- curl --connect-timeout 3 rebel-base.default.svc.north.local)"
	echo "Ping from north to south using FQDN (of south - will fail due to policy): $$(kubectl --context north exec -ti deployment/tie-fighter -- curl --connect-timeout 3 rebel-base.default.svc.south.local)"

	kubectl --context north delete -f $(TEMPDIR)/policy1.yaml
	kubectl --context south delete -f $(TEMPDIR)/policy1.yaml
	kubectl --context south delete -f $(TEMPDIR)/policy2.yaml

	echo "Ping from north to south using FQDN (of north - works now policy removed): $$(kubectl --context north exec -ti deployment/tie-fighter -- curl --connect-timeout 3 rebel-base.default.svc.north.local)"
	echo "Ping from north to south using FQDN (of south - works now policy removed): $$(kubectl --context north exec -ti deployment/tie-fighter -- curl --connect-timeout 3 rebel-base.default.svc.south.local)"

	kubectl --context north delete -f $(TEMPDIR)/cluster1.yaml
	kubectl --context south delete -f $(TEMPDIR)/cluster2.yaml
	@rm -rf $(TEMPDIR)


define MULTICLUSTER_TEST1
---
apiVersion: v1
kind: Service
metadata:
  name: rebel-base
  annotations:
    service.cilium.io/global: "true"
    service.cilium.io/shared: "false"
    service.cilium.io/global-sync-endpoint-slices: "true"
spec:
  type: ClusterIP
  ports:
  - port: 80
  selector:
    name: rebel-base
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: x-wing
spec:
  selector:
    matchLabels:
      name: x-wing
  replicas: 2
  template:
    metadata:
      labels:
        name: x-wing
    spec:
      containers:
      - name: x-wing-container
        image: quay.io/cilium/json-mock:v1.3.3@sha256:f26044a2b8085fcaa8146b6b8bb73556134d7ec3d5782c6a04a058c945924ca0
        livenessProbe:
          exec:
            command:
            - curl
            - -sS
            - -o
            - /dev/null
            - localhost
        readinessProbe:
          exec:
            command:
            - curl
            - -sS
            - -o
            - /dev/null
            - localhost

---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: tie-fighter
spec:
  selector:
    matchLabels:
      name: tie-fighter
  replicas: 2
  template:
    metadata:
      labels:
        name: tie-fighter
    spec:
      containers:
      - name: tie-fighter-container
        image: quay.io/cilium/json-mock:v1.3.3@sha256:f26044a2b8085fcaa8146b6b8bb73556134d7ec3d5782c6a04a058c945924ca0
        livenessProbe:
          exec:
            command:
            - curl
            - -sS
            - -o
            - /dev/null
            - localhost
        readinessProbe:
          exec:
            command:
            - curl
            - -sS
            - -o
            - /dev/null
            - localhost


endef
export MULTICLUSTER_TEST1

define MULTICLUSTER_TEST2
---
apiVersion: v1
kind: Service
metadata:
  name: rebel-base
  annotations:
    service.cilium.io/global: "true"
    service.cilium.io/shared: "true"
spec:
  type: ClusterIP
  ports:
  - port: 80
  selector:
    name: rebel-base
---
apiVersion: v1
kind: Service
metadata:
  name: rebel-base-headless
  annotations:
    service.cilium.io/global: "true"
    service.cilium.io/shared: "true"
    service.cilium.io/global-sync-endpoint-slices: "true"
spec:
  type: ClusterIP
  clusterIP: None
  ports:
  - port: 80
  selector:
    name: rebel-base
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: rebel-base
spec:
  selector:
    matchLabels:
      name: rebel-base
  replicas: 2
  template:
    metadata:
      labels:
        name: rebel-base
    spec:
      containers:
      - name: rebel-base
        image: docker.io/nginx:1.15.8
        volumeMounts:
          - name: html
            mountPath: /usr/share/nginx/html/
        livenessProbe:
          httpGet:
            path: /
            port: 80
          periodSeconds: 1
        readinessProbe:
          httpGet:
            path: /
            port: 80
      volumes:
        - name: html
          configMap:
            name: rebel-base-response
            items:
              - key: message
                path: index.html
---
apiVersion: v1
kind: ConfigMap
metadata:
  name: rebel-base-response
data:
  message: "{\"Galaxy\": \"Alderaan\", \"Cluster\": \"Cluster-2\"}\n"
---
apiVersion: apps/v1
kind: Deployment
metadata:
  name: x-wing
spec:
  selector:
    matchLabels:
      name: x-wing
  replicas: 2
  template:
    metadata:
      labels:
        name: x-wing
    spec:
      containers:
      - name: x-wing-container
        image: quay.io/cilium/json-mock:v1.3.3@sha256:f26044a2b8085fcaa8146b6b8bb73556134d7ec3d5782c6a04a058c945924ca0
        livenessProbe:
          exec:
            command:
            - curl
            - -sS
            - -o
            - /dev/null
            - localhost
        readinessProbe:
          exec:
            command:
            - curl
            - -sS
            - -o
            - /dev/null
            - localhost

endef
export MULTICLUSTER_TEST2


define MULTICLUSTER_POLICY1
---
apiVersion: cilium.io/v2
kind: CiliumNetworkPolicy
metadata:
  name: database-policy
  namespace: default
spec:
  endpointSelector:
    matchLabels:
      name: rebel-base
  ingress:
    - {}
    - fromEndpoints:
        - matchLabels:
            name: x-wing
      toPorts:
        - ports:
            - port: "80"
  egress:
    - {}

endef
export MULTICLUSTER_POLICY1



define MULTICLUSTER_POLICY2
---
# https://docs.cilium.io/en/stable/network/clustermesh/policy/#gs-clustermesh-network-policy
apiVersion: "cilium.io/v2"
kind: CiliumNetworkPolicy
metadata:
  name: "allow-cross-cluster"
spec:
  description: "Allow x-wing in north to contact rebel-base in south"
  endpointSelector:
    matchLabels:
      name: x-wing
      io.cilium.k8s.policy.cluster: north
  egress:
  - toEndpoints:
    - matchLabels:
        name: rebel-base
        io.cilium.k8s.policy.cluster: south

endef
export MULTICLUSTER_POLICY2

